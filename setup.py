import sys
import distutils

from setuptools import setup


given_ldflags = distutils.sysconfig.get_config_var('LDSHARED')
sys.stderr.write("Appending -L/usr/lib to default LDSHARED\n")
distutils.sysconfig.get_config_vars()
distutils.sysconfig._config_vars['LDSHARED'] = given_ldflags.replace(' -L', ' -L/usr/lib -L', 1)


setup(
    name='pystarlingencoding',
    version='0.1',
    url='',
    license='',
    author='Phil Krylov',
    author_email='phil.krylov@gmail.com',
    description='Python codec for Starling legacy encoding',
    packages=['pystarlingencoding'],
    install_requires=['cffi>=1.0.0'],
    setup_requires=['cffi>=1.0.0'],
    cffi_modules=['pystarlingencoding/extension_build.py:ffibuilder'],
)
