#include <stdlib.h>
#include <string.h>

#include "trie.h"

#define Align Cell


static inline int align_size(int size, int alignment) {
    return (size + alignment - 1) & ~(alignment - 1);
}


DumbTrie *dumb_trie_new(void) {
    DumbTrie *trie = calloc(1, sizeof(DumbTrie));
    trie->end = '\xFF';
    trie->data = 1;
    return trie;
}


static inline const Cell *dumb_trie_target_cell(const DumbTrie *trie, uint8_t c) {
    return &trie->children[c];
}


static DumbTrie *dumb_trie_real_add(DumbTrie *root, int block_no, const uint8_t *key, int len, Cell data) {
    if (len) {
        int next_block_no = *dumb_trie_target_cell(&root[block_no], key[0]);
        if (next_block_no == 0) {
            next_block_no = root->data;
            root = realloc(root, (next_block_no + 1) * sizeof(DumbTrie));
            DumbTrie *next_node = &root[next_block_no];
            memset(next_node, 0, sizeof(DumbTrie));
            root->data = next_block_no + 1;
            next_node->end = '\xFF';
            *(Cell *)dumb_trie_target_cell(&root[block_no], key[0]) = next_block_no;
        }
        return dumb_trie_real_add(root, next_block_no, key + 1, len - 1, data);
    } else {
        root[block_no].data = data;
        return root;
    }
}


DumbTrie *dumb_trie_add(DumbTrie *trie, const uint8_t *key, int len, Cell data) {
    return dumb_trie_real_add(trie, 0, key, len, data);
}


static void dumb_trie_get_extent(const DumbTrie *dumb_node, uint8_t *start_ptr, uint8_t *end_ptr) {
    unsigned start, end;
    for (start = dumb_node->start;
         start <= dumb_node->end && !dumb_node->children[start - dumb_node->start];
         start++);
    if (start <= dumb_node->end) {
        unsigned n;
        end = n = start;
        while (++n && n <= dumb_node->end) {
            if (dumb_node->children[n - dumb_node->start]) {
                end = n;
            }
        }
    } else {
        end = 0;
        start = 1;
    }
    *start_ptr = start;
    *end_ptr = end;
}


static inline int trie_block_size(unsigned start, unsigned end) {
    return align_size(sizeof(Trie) + sizeof(Cell) * (end - start + 1),
                      sizeof(Align));
}


static Trie *trie_new(int root_block_size) {
    return (Trie *)calloc(1, root_block_size);
}


static inline const Cell *trie_target_cell(const Trie *trie, uint8_t c) {
    return &trie->children[c - trie->start];
}


Trie *dumb_trie_compact(const DumbTrie *dumb_trie) {
    uint16_t block_map[dumb_trie->data];
    const DumbTrie *dumb_trie_end = &dumb_trie[dumb_trie->data];
    const DumbTrie *dumb_node;
    Trie *trie;
    /* First pass: allocate everything, populate block_map */
    for (dumb_node = dumb_trie; dumb_node < dumb_trie_end; dumb_node++) {
        uint8_t start, end;
        Trie *new_node;
        dumb_trie_get_extent(dumb_node, &start, &end);
        int block_size = trie_block_size(start, end);
        if (dumb_node == dumb_trie) {
            trie = trie_new(block_size);
            new_node = trie;
            block_map[0] = 0;
        } else {
            trie = realloc(trie, trie->data * sizeof(Align) + block_size);
            block_map[dumb_node - dumb_trie] = trie->data;
            new_node = (Trie *)&((Align *)trie)[trie->data];
            memset(new_node, 0, block_size);
            new_node->data = dumb_node->data;
        }
        trie->data += block_size / sizeof(Align);
        new_node->start = start;
        new_node->end = end;
    }
    /* Second pass: convert children */
    Trie *node = trie;
    for (dumb_node = dumb_trie;
         dumb_node < dumb_trie_end;
         dumb_node++, node = (Trie *)(((uint8_t *)node) + trie_block_size(node->start, node->end))) {
        int n;
        for (n = node->start; n <= node->end; n++) {
            if (dumb_node->children[n]) {
                *(Cell *)trie_target_cell(node, n) = block_map[*dumb_trie_target_cell(dumb_node, n)];
            }
        }
    }
    return trie;
}


Cell trie_find(const uint8_t *addr, int len, const Trie *trie) {
    int n_child_block;
    while (len &&
           trie->start <= *addr &&
           *addr <= trie->end &&
           (n_child_block = *trie_target_cell(trie, *addr))) {
        trie = (Trie *)&((Align *)trie)[n_child_block];
        addr++, len--;
    }
    return len ? 0 : trie->data;
}


Cell trie_find_prefix(const uint8_t *addr, int len, const Trie *trie) {
    const Trie *root = trie;
    int n_child_block;
    Cell good_data = 0;
    while (len &&
           trie->start <= *addr &&
           *addr <= trie->end &&
           (n_child_block = *trie_target_cell(trie, *addr))) {
        trie = (Trie *)&(((Align *)root)[n_child_block]);
        good_data = (!good_data || trie->data) ? trie->data : good_data;
        addr++, len--;
    }
    return good_data;
}


void dumb_trie_free(DumbTrie *trie) {
    free(trie);
}


void trie_free(Trie *trie) {
    free(trie);
}


#ifdef BUILD_TEST
#include <stdio.h>
int main(int argc, const char **argv) {
   if (argc > 2) {
      int id = 1;
      const char *prefix = argv[1];

      DumbTrie *dumb_trie = dumb_trie_new();
      printf("Trie created, size %zu\n", dumb_trie->data * sizeof(DumbTrie));

      for (id = 1; id + 1 < argc; id++) {
          dumb_trie = dumb_trie_add(dumb_trie, (const uint8_t *)argv[id + 1], strlen(argv[id + 1]), id);
      }
      printf("Trie populated, size %zu\n", dumb_trie->data * sizeof(DumbTrie));

      Trie *trie = dumb_trie_compact(dumb_trie);
      trie_free((Trie *)dumb_trie);
      printf("Trie compacted, size %zu\n", trie->data * sizeof(Align));

      printf("Found: %d\n", trie_find_prefix((const uint8_t *)prefix, strlen(prefix), trie));

      trie_free(trie);
   }
}
#endif