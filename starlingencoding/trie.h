#ifndef TRIE_H
#define TRIE_H

typedef uint16_t Cell;

typedef struct {
    uint8_t start, end;
    Cell data;
    Cell children[];
} Trie;

typedef struct {
    uint8_t start, end;
    Cell data;
    Cell children[256];
} DumbTrie;

DumbTrie *dumb_trie_new(void);
DumbTrie *dumb_trie_add(DumbTrie *trie, const uint8_t *key, int len, Cell data);
Trie *dumb_trie_compact(const DumbTrie *dumb_trie);
void dumb_trie_free(DumbTrie *trie);
Cell trie_find(const uint8_t *addr, int len, const Trie *trie);
Cell trie_find_prefix(const uint8_t *addr, int len, const Trie *trie);
void trie_free(Trie *trie);

#endif