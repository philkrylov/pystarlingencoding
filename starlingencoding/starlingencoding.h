#ifndef _STARLINGENCODING_H
#define _STARLINGENCODING_H

uint8_t *starlingencoding_to_utf8(const uint8_t *, size_t);
wchar_t *starlingencoding_to_wchar_t(const uint8_t *, size_t);
void starlingencoding_free_utf8(uint8_t *);
void starlingencoding_free_wchar_t(wchar_t *);

#endif