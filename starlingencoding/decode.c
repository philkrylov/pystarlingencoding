#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#ifdef __WIN32__
#include <windows.h>
#else
#include <iconv.h>
#endif

#include "starlingencoding.h"
#include "trie.h"

struct {
    const char *key;
    const wchar_t value[3];
    uint8_t utf8_len;
    uint8_t utf8[6];
} starlingencoding_table[] = {
    {"!", {0x0021, 0}},
    {"\"", {0x0022, 0}},
    {"#", {0x0023, 0}},
    {"$", {0x0024, 0}},
    {"%", {0x0025, 0}},
    {"&", {0x0026, 0}},
    {"\'", {0x0027, 0}},
    {"(", {0x0028, 0}},
    {")", {0x0029, 0}},
    {"*", {0x002A, 0}},
    {"+", {0x002B, 0}},
    {",", {0x002C, 0}},
    {"-", {0x002D, 0}},
    {".", {0x002E, 0}},
    {"/", {0x002F, 0}},
    {"0", {0x0030, 0}},
    {"1", {0x0031, 0}},
    {"2", {0x0032, 0}},
    {"3", {0x0033, 0}},
    {"4", {0x0034, 0}},
    {"5", {0x0035, 0}},
    {"6", {0x0036, 0}},
    {"7", {0x0037, 0}},
    {"8", {0x0038, 0}},
    {"9", {0x0039, 0}},
    {":", {0x003A, 0}},
    {";", {0x003B, 0}},
    {"<", {0x003C, 0}},
    {"=", {0x003D, 0}},
    {">", {0x003E, 0}},
    {"?", {0x003F, 0}},
    {"@", {0x0040, 0}},
    {"A", {0x0041, 0}},
    {"A\xDB", {0x00C0, 0}},  /* LATIN CAPITAL LETTER A WITH GRAVE */
    {"A\xB1", {0x00C1, 0}},  /* LATIN CAPITAL LETTER A WITH ACUTE */
    {"A^", {0x00C2, 0}},  /* LATIN CAPITAL LETTER A WITH CIRCUMFLEX */
    {"A~", {0x00C3, 0}},  /* LATIN CAPITAL LETTER A WITH TILDE */
    {"A\xBC", {0x00C4, 0}},  /* LATIN CAPITAL LETTER A WITH DIAERESIS */
    {"A\xC8", {0x00C5, 0}},  /* LATIN CAPITAL LETTER A WITH RING */
    {"A\xC4", {0x0100, 0}},  /* LATIN CAPITAL LETTER A WITH MACRON */
    {"A\xDF", {0x0102, 0}},  /* LATIN CAPITAL LETTER A WITH BREVE */
    {"A\xB3", {0x0104, 0}},  /* LATIN CAPITAL LETTER A WITH OGONEK */
    {"A\xF6", {0x01CD, 0}},  /* LATIN CAPITAL LETTER A WITH CARON */
    {"A\xBB", {0x0226, 0}},  /* LATIN CAPITAL LETTER A WITH DOT ABOVE */
    {"A^\xB1", {0x1EA4, 0}},  /* LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND ACUTE */
    {"A^\xDB", {0x1EA6, 0}},  /* LATIN CAPITAL LETTER A WITH CIRCUMFLEX AND GRAVE */
    {"A\xDF\xB1", {0x1EAE, 0}},  /* LATIN CAPITAL LETTER A WITH BREVE AND ACUTE */
    {"A\xDF\xDB", {0x1EB0, 0}},  /* LATIN CAPITAL LETTER A WITH BREVE AND GRAVE */
    {"B", {0x0042, 0}},
    {"B\xDC", {0x1E04, 0}},  /* LATIN CAPITAL LETTER B WITH DOT BELOW */
    {"C", {0x0043, 0}},
    {"C\x1D\xB3", {0x00C7, 0}},  /* LATIN CAPITAL LETTER C WITH CEDILLA */
    {"C\xB3", {0x00C7, 0}},
    {"C\xB1", {0x0106, 0}},  /* LATIN CAPITAL LETTER C WITH ACUTE */
    {"C^", {0x0108, 0}},  /* LATIN CAPITAL LETTER C WITH CIRCUMFLEX */
    {"C\xBB", {0x010A, 0}},  /* LATIN CAPITAL LETTER C WITH DOT ABOVE */
    {"C\xF6", {0x010C, 0}},  /* LATIN CAPITAL LETTER C WITH CARON */
    {"D", {0x0044, 0}},
    {"D\xF6", {0x010E, 0}},  /* LATIN CAPITAL LETTER D WITH CARON */
    {"D_", {0x1E0E, 0}},  /* LATIN CAPITAL LETTER D WITH LINE BELOW */
    {"E", {0x0045, 0}},
    {"E\xDB", {0x00C8, 0}},  /* LATIN CAPITAL LETTER E WITH GRAVE */
    {"E\xB1", {0x00C9, 0}},  /* LATIN CAPITAL LETTER E WITH ACUTE */
    {"E^", {0x00CA, 0}},  /* LATIN CAPITAL LETTER E WITH CIRCUMFLEX */
    {"E\xBC", {0x00CB, 0}},  /* LATIN CAPITAL LETTER E WITH DIAERESIS */
    {"E\xC4", {0x0112, 0}},  /* LATIN CAPITAL LETTER E WITH MACRON */
    {"E\xDF", {0x0114, 0}},  /* LATIN CAPITAL LETTER E WITH BREVE */
    {"E\xBB", {0x0116, 0}},  /* LATIN CAPITAL LETTER E WITH DOT ABOVE */
    {"E\xB3", {0x0118, 0}},  /* LATIN CAPITAL LETTER E WITH OGONEK */
    {"E\xF6", {0x011A, 0}},  /* LATIN CAPITAL LETTER E WITH CARON */
    {"E\xC4\xB1", {0x1E16, 0}},  /* LATIN CAPITAL LETTER E WITH MACRON AND ACUTE */
    {"E\x1D~", {0x1E1A, 0}},  /* LATIN CAPITAL LETTER E WITH TILDE BELOW */
    {"F", {0x0046, 0}},
    {"G", {0x0047, 0}},
    {"G^", {0x011C, 0}},  /* LATIN CAPITAL LETTER G WITH CIRCUMFLEX */
    {"G\xDF", {0x011E, 0}},  /* LATIN CAPITAL LETTER G WITH BREVE */
    {"G\xBB", {0x0120, 0}},  /* LATIN CAPITAL LETTER G WITH DOT ABOVE */
    {"G\x1D\xB3", {0x0122, 0}},  /* LATIN CAPITAL LETTER G WITH CEDILLA */
    {"G\xF6", {0x01E6, 0}},  /* LATIN CAPITAL LETTER G WITH CARON */
    {"G\xB1", {0x01F4, 0}},  /* LATIN CAPITAL LETTER G WITH ACUTE */
    {"H", {0x0048, 0}},
    {"H^", {0x0124, 0}},  /* LATIN CAPITAL LETTER H WITH CIRCUMFLEX */
    {"H\xDC", {0x1E24, 0}},  /* LATIN CAPITAL LETTER H WITH DOT BELOW */
    {"I", {0x0049, 0}},
    {"I\xDB", {0x00CC, 0}},  /* LATIN CAPITAL LETTER I WITH GRAVE */
    {"I\xB1", {0x00CD, 0}},  /* LATIN CAPITAL LETTER I WITH ACUTE */
    {"I^", {0x00CE, 0}},  /* LATIN CAPITAL LETTER I WITH CIRCUMFLEX */
    {"I\xBC", {0x00CF, 0}},  /* LATIN CAPITAL LETTER I WITH DIAERESIS */
    {"I~", {0x0128, 0}},  /* LATIN CAPITAL LETTER I WITH TILDE */
    {"I\xC4", {0x012A, 0}},  /* LATIN CAPITAL LETTER I WITH MACRON */
    {"I\xDF", {0x012C, 0}},  /* LATIN CAPITAL LETTER I WITH BREVE */
    {"I\xB3", {0x012E, 0}},  /* LATIN CAPITAL LETTER I WITH OGONEK */
    {"I\xBB", {0x0130, 0}},  /* LATIN CAPITAL LETTER I WITH DOT ABOVE */
    {"I\xF6", {0x01CF, 0}},  /* LATIN CAPITAL LETTER I WITH CARON */
    {"I\x1D~", {0x1E2C, 0}},  /* LATIN CAPITAL LETTER I WITH TILDE BELOW */
    {"I\xDC", {0x1ECA, 0}},  /* LATIN CAPITAL LETTER I WITH DOT BELOW */
    {"J", {0x004A, 0}},
    {"J^", {0x0134, 0}},  /* LATIN CAPITAL LETTER J WITH CIRCUMFLEX */
    {"K", {0x004B, 0}},
    {"K\x1D\xB3", {0x0136, 0}},  /* LATIN CAPITAL LETTER K WITH CEDILLA */
    {"K\xB1", {0x1E30, 0}},  /* LATIN CAPITAL LETTER K WITH ACUTE */
    {"K_", {0x1E34, 0}},  /* LATIN CAPITAL LETTER K WITH LINE BELOW */
    {"L", {0x004C, 0}},
    {"L\xB1", {0x0139, 0}},  /* LATIN CAPITAL LETTER L WITH ACUTE */
    {"L\x1D\xB3", {0x013B, 0}},  /* LATIN CAPITAL LETTER L WITH CEDILLA */
    {"L\xF6", {0x013D, 0}},  /* LATIN CAPITAL LETTER L WITH CARON */
    {"L\xDC", {0x1E36, 0}},  /* LATIN CAPITAL LETTER L WITH DOT BELOW */
    {"L_", {0x1E3A, 0}},  /* LATIN CAPITAL LETTER L WITH LINE BELOW */
    {"M", {0x004D, 0}},
    {"M\xB1", {0x1E3E, 0}},  /* LATIN CAPITAL LETTER M WITH ACUTE */
    {"M\xBB", {0x1E40, 0}},  /* LATIN CAPITAL LETTER M WITH DOT ABOVE */
    {"M\xDC", {0x1E42, 0}},  /* LATIN CAPITAL LETTER M WITH DOT BELOW */
    {"N", {0x004E, 0}},
    {"N~", {0x00D1, 0}},  /* LATIN CAPITAL LETTER N WITH TILDE */
    {"N\xB1", {0x0143, 0}},  /* LATIN CAPITAL LETTER N WITH ACUTE */
    {"N\x1D\xB3", {0x0145, 0}},  /* LATIN CAPITAL LETTER N WITH CEDILLA */
    {"N\xF6", {0x0147, 0}},  /* LATIN CAPITAL LETTER N WITH CARON */
    {"N\xBF", {0x014A, 0}},  /* LATIN CAPITAL LETTER ENG */
    {"N\xDB", {0x01F8, 0}},  /* LATIN CAPITAL LETTER N WITH GRAVE */
    {"N\xBB", {0x1E44, 0}},  /* LATIN CAPITAL LETTER N WITH DOT ABOVE */
    {"N\xDC", {0x1E46, 0}},  /* LATIN CAPITAL LETTER N WITH DOT BELOW */
    {"N_", {0x1E48, 0}},  /* LATIN CAPITAL LETTER N WITH LINE BELOW */
    {"O", {0x004F, 0}},
    {"O\xDB", {0x00D2, 0}},  /* LATIN CAPITAL LETTER O WITH GRAVE */
    {"O\xB1", {0x00D3, 0}},  /* LATIN CAPITAL LETTER O WITH ACUTE */
    {"O^", {0x00D4, 0}},  /* LATIN CAPITAL LETTER O WITH CIRCUMFLEX */
    {"O~", {0x00D5, 0}},  /* LATIN CAPITAL LETTER O WITH TILDE */
    {"O\xBC", {0x00D6, 0}},  /* LATIN CAPITAL LETTER O WITH DIAERESIS */
    {"O\xC4", {0x014C, 0}},  /* LATIN CAPITAL LETTER O WITH MACRON */
    {"O\xDF", {0x014E, 0}},  /* LATIN CAPITAL LETTER O WITH BREVE */
    {"O\x1D\xB1", {0x0150, 0}},  /* LATIN CAPITAL LETTER O WITH DOUBLE ACUTE */
    {"O\xF6", {0x01D1, 0}},  /* LATIN CAPITAL LETTER O WITH CARON */
    {"O\xB3", {0x01EA, 0}},  /* LATIN CAPITAL LETTER O WITH OGONEK */
    {"O\x1D\xDB", {0x020C, 0}},  /* LATIN CAPITAL LETTER O WITH DOUBLE GRAVE */
    {"O\xBB", {0x022E, 0}},  /* LATIN CAPITAL LETTER O WITH DOT ABOVE */
    {"O~\xB1", {0x1E4C, 0}},  /* LATIN CAPITAL LETTER O WITH TILDE AND ACUTE */
    {"O\xC4\xB1", {0x1E52, 0}},  /* LATIN CAPITAL LETTER O WITH MACRON AND ACUTE */
    {"O\xDC", {0x1ECC, 0}},  /* LATIN CAPITAL LETTER O WITH DOT BELOW */
    {"P", {0x0050, 0}},
    {"P\xB1", {0x1E54, 0}},  /* LATIN CAPITAL LETTER P WITH ACUTE */
    {"P\xBB", {0x1E56, 0}},  /* LATIN CAPITAL LETTER P WITH DOT ABOVE */
    {"Q", {0x0051, 0}},
    {"R", {0x0052, 0}},
    {"R\xB1", {0x0154, 0}},  /* LATIN CAPITAL LETTER R WITH ACUTE */
    {"R\x1D\xB3", {0x0156, 0}},  /* LATIN CAPITAL LETTER R WITH CEDILLA */
    {"R\xF6", {0x0158, 0}},  /* LATIN CAPITAL LETTER R WITH CARON */
    {"R\xBB", {0x1E58, 0}},  /* LATIN CAPITAL LETTER P WITH DOT ABOVE */
    {"R\xDC", {0x1E5A, 0}},  /* LATIN CAPITAL LETTER R WITH DOT BELOW */
    {"R_", {0x1E5E, 0}},  /* LATIN CAPITAL LETTER R WITH LINE BELOW */
    {"S", {0x0053, 0}},
    {"S\xB1", {0x015A, 0}},  /* LATIN CAPITAL LETTER S WITH ACUTE */
    {"S^", {0x015C, 0}},  /* LATIN CAPITAL LETTER S WITH CIRCUMFLEX */
    {"S\x1D\xB3", {0x015E, 0}},  /* LATIN CAPITAL LETTER S WITH CEDILLA */
    {"S\xF6", {0x0160, 0}},  /* LATIN CAPITAL LETTER S WITH CARON */
    {"T", {0x0054, 0}},
    {"T\x1D\xB3", {0x0162, 0}},  /* LATIN CAPITAL LETTER T WITH CEDILLA */
    {"T\xF6", {0x0164, 0}},  /* LATIN CAPITAL LETTER T WITH CARON */
    {"T\xBB", {0x1E6A, 0}},  /* LATIN CAPITAL LETTER T WITH DOT ABOVE */
    {"T\xDC", {0x1E6C, 0}},  /* LATIN CAPITAL LETTER T WITH DOT BELOW */
    {"T_", {0x1E6E, 0}},  /* LATIN CAPITAL LETTER T WITH LINE BELOW */
    {"U", {0x0055, 0}},
    {"U\xDB", {0x00D9, 0}},  /* LATIN CAPITAL LETTER U WITH GRAVE */
    {"U\xB1", {0x00DA, 0}},  /* LATIN CAPITAL LETTER U WITH ACUTE */
    {"U^", {0x00DB, 0}},  /* LATIN CAPITAL LETTER U WITH CIRCUMFLEX */
    {"U\xBC", {0x00DC, 0}},  /* LATIN CAPITAL LETTER U WITH DIAERESIS */
    {"U~", {0x0168, 0}},  /* LATIN CAPITAL LETTER U WITH TILDE */
    {"U\xC4", {0x016A, 0}},  /* LATIN CAPITAL LETTER U WITH MACRON */
    {"U\xDF", {0x016C, 0}},  /* LATIN CAPITAL LETTER U WITH BREVE */
    {"U\xC8", {0x016E, 0}},  /* LATIN CAPITAL LETTER U WITH RING */
    {"U\x1D\xB1", {0x0170, 0}},  /* LATIN CAPITAL LETTER U WITH DOUBLE ACUTE */
    {"U\xB3", {0x0172, 0}},  /* LATIN CAPITAL LETTER U WITH OGONEK */
    {"U\xF6", {0x01D3, 0}},  /* LATIN CAPITAL LETTER U WITH CARON */
    {"U\x1D~", {0x1E74, 0}},  /* LATIN CAPITAL LETTER U WITH TILDE BELOW */
    {"U~\xB1", {0x1E78, 0}},  /* LATIN CAPITAL LETTER U WITH TILDE AND ACUTE */
    {"V", {0x0056, 0}},
    {"V~", {0x1E7C, 0}},  /* LATIN CAPITAL LETTER V WITH TILDE */
    {"W", {0x0057, 0}},
    {"W^", {0x0174, 0}},  /* LATIN CAPITAL LETTER W WITH CIRCUMFLEX */
    {"X", {0x0058, 0}},
    {"Y", {0x0059, 0}},
    {"Y\xB1", {0x00DD, 0}},  /* LATIN CAPITAL LETTER Y WITH ACUTE */
    {"Y^", {0x0176, 0}},  /* LATIN CAPITAL LETTER Y WITH CIRCUMFLEX */
    {"Y\xBC", {0x0178, 0}},  /* LATIN CAPITAL LETTER Y WITH DIAERESIS */
    {"Y\xC4", {0x0232, 0}},  /* LATIN CAPITAL LETTER Y WITH MACRON */
    {"Z", {0x005A, 0}},
    {"Z\xB1", {0x0179, 0}},  /* LATIN CAPITAL LETTER Z WITH ACUTE */
    {"Z\xBB", {0x017B, 0}},  /* LATIN CAPITAL LETTER Z WITH DOT ABOVE */
    {"Z\xF6", {0x017D, 0}},  /* LATIN CAPITAL LETTER Z WITH CARON */
    {"Z^", {0x1E90, 0}},  /* LATIN SMALL LETTER Z WITH CIRCUMFLEX */
    {"Z\xDC", {0x1E92, 0}},  /* LATIN SMALL LETTER Z WITH DOT BELOW */
    {"[", {0x005B, 0}},
    {"\\", {0x005C, 0}},
    {"]", {0x005D, 0}},
    {"^", {0x0302, 0}},  /* COMBINING CIRCUMFLEX ACCENT */
    {"_", {0x0332, 0}},  /* COMBINING LOW LINE */
    {"`", {0x0060, 0}},
    {"a", {0x0061, 0}},
    {"a\xDB", {0x00E0, 0}},  /* LATIN SMALL LETTER A WITH GRAVE */
    {"a\xB1", {0x00E1, 0}},  /* LATIN SMALL LETTER A WITH ACUTE */
    {"a^", {0x00E2, 0}},  /* LATIN SMALL LETTER A WITH CIRCUMFLEX */
    {"a~", {0x00E3, 0}},  /* LATIN SMALL LETTER A WITH TILDE */
    {"a\xC8", {0x00E5, 0}},  /* LATIN SMALL LETTER A WITH RING ABOVE */
    {"\xB0", {0x0101, 0}},  /* LATIN SMALL LETTER A WITH MACRON */
    {"a\xC4", {0x0101, 0}},  /* LATIN SMALL LETTER A WITH MACRON */
    {"a\xDF", {0x0103, 0}},  /* LATIN SMALL LETTER A WITH BREVE */
    {"a\xB3", {0x0105, 0}},  /* LATIN SMALL LETTER A WITH OGONEK */
    {"a\xF6", {0x01CE, 0}},  /* LATIN SMALL LETTER A WITH CARON */
    {"a\x1D\xDB", {0x0201, 0}},  /* LATIN SMALL LETTER A WITH DOUBLE GRAVE */
    {"a\x1D^", {0x0203, 0}},  /* LATIN SMALL LETTER A WITH INVERTED BREVE ABOVE */
    {"a\xBB", {0x0227, 0}},  /* LATIN SMALL LETTER A WITH DOT ABOVE */
    {"a\xDC", {0x1EA1, 0}},  /* LATIN SMALL LETTER A WITH DOT BELOW */
    {"a^\xB1", {0x1EA5, 0}},  /* LATIN SMALL LETTER A WITH CIRCUMFLEX AND ACUTE */
    {"a^\xDB", {0x1EA7, 0}},  /* LATIN SMALL LETTER A WITH CIRCUMFLEX AND GRAVE */
    {"a\xDF\xB1", {0x1EAF, 0}},  /* LATIN SMALL LETTER A WITH BREVE AND ACUTE */
    {"a\xDF\xDB", {0x1EB1, 0}},  /* LATIN SMALL LETTER A WITH BREVE AND GRAVE */
    {"b", {0x0062, 0}},
    {"b\xDC", {0x1E05, 0}},  /* LATIN SMALL LETTER B WITH DOT BELOW */
    {"c", {0x0063, 0}},
    {"c\xB1", {0x0107, 0}},  /* LATIN SMALL LETTER C WITH ACUTE */
    {"c^", {0x0109, 0}},  /* LATIN SMALL LETTER C WITH CIRCUMFLEX */
    {"c\xBB", {0x010B, 0}},  /* LATIN SMALL LETTER C WITH DOT ABOVE */
    {"d", {0x0064, 0}},
    {"d\xF6", {0x010F, 0}},  /* LATIN SMALL LETTER D WITH CARON */
    {"d\xDC", {0x1E0D, 0}},  /* LATIN SMALL LETTER D WITH DOT BELOW */
    {"d_", {0x1E0F, 0}},  /* LATIN SMALL LETTER D WITH LINE BELOW */
    {"e", {0x0065, 0}},
    {"e\xDB", {0x00E8, 0}},  /* LATIN SMALL LETTER E WITH GRAVE */
    {"e\xB1", {0x00E9, 0}},  /* LATIN SMALL LETTER E WITH ACUTE */
    {"e^", {0x00EA, 0}},  /* LATIN SMALL LETTER E WITH CIRCUMFLEX */
    {"e\xBC", {0x00EB, 0}},  /* LATIN SMALL LETTER E WITH DIAERESIS */
    {"e\xDF", {0x0115, 0}},  /* LATIN SMALL LETTER E WITH BREVE */
    {"e\xBB", {0x0117, 0}},  /* LATIN SMALL LETTER E WITH DOT ABOVE */
    {"e\xB3", {0x0119, 0}},  /* LATIN SMALL LETTER E WITH OGONEK */
    {"e\xF6", {0x011B, 0}},  /* LATIN SMALL LETTER E WITH CARON */
    {"e\x1D\xDB", {0x0205, 0}},  /* LATIN SMALL LETTER E WITH DOUBLE GRAVE */
    {"e\x1D^", {0x0207, 0}},  /* LATIN SMALL LETTER E WITH INVERTED BREVE ABOVE */
    {"e\xC4\xB1", {0x1E17, 0}},  /* LATIN SMALL LETTER E WITH MACRON AND ACUTE */
    {"e\x1D~", {0x1E1B, 0}},  /* LATIN SMALL LETTER E WITH TILDE BELOW */
    {"e\xDC", {0x1EB9, 0}},  /* LATIN SMALL LETTER E WITH DOT BELOW */
    {"e~", {0x1EBD, 0}},  /* LATIN SMALL LETTER E WITH TILDE */
    {"f", {0x0066, 0}},
    {"g", {0x0067, 0}},
    {"g^", {0x011D, 0}},  /* LATIN SMALL LETTER G WITH CIRCUMFLEX */
    {"g\xDF", {0x011F, 0}},  /* LATIN SMALL LETTER G WITH BREVE */
    {"g\xBB", {0x0121, 0}},  /* LATIN SMALL LETTER G WITH DOT ABOVE */
    {"g\x1D\xB3", {0x0123, 0}},  /* LATIN SMALL LETTER G WITH CEDILLA */
    {"g\xF6", {0x01E7, 0}},  /* LATIN SMALL LETTER G WITH CARON */
    {"g\xB1", {0x01F5, 0}},  /* LATIN SMALL LETTER G WITH ACUTE */
    {"h", {0x0068, 0}},
    {"h^", {0x0125, 0}},  /* LATIN SMALL LETTER H WITH CIRCUMFLEX */
    {"h\xDC", {0x1E25, 0}},  /* LATIN SMALL LETTER H WITH DOT BELOW */
    {"h_", {0x1E96, 0}},  /* LATIN SMALL LETTER H WITH LINE BELOW */
    {"i", {0x0069, 0}},
    {"i\xDB", {0x00EC, 0}},  /* LATIN SMALL LETTER I WITH GRAVE */
    {"i\xB1", {0x00ED, 0}},  /* LATIN SMALL LETTER I WITH ACUTE */
    {"i^", {0x00EE, 0}},  /* LATIN SMALL LETTER I WITH CIRCUMFLEX */
    {"i\xBC", {0x00EF, 0}},  /* LATIN SMALL LETTER I WITH DIAERESIS */
    {"i~", {0x0129, 0}},  /* LATIN SMALL LETTER I WITH TILDE */
    {"i\xDF", {0x012D, 0}},  /* LATIN SMALL LETTER I WITH BREVE */
    {"i\xB3", {0x012F, 0}},  /* LATIN SMALL LETTER I WITH OGONEK */
    {"i\xF6", {0x01D0, 0}},  /* LATIN SMALL LETTER I WITH CARON */
    {"i\x1D\xDB", {0x0209, 0}},  /* LATIN SMALL LETTER I WITH DOUBLE GRAVE */
    {"i\x1D~", {0x1E2D, 0}},  /* LATIN SMALL LETTER I WITH TILDE BELOW */
    {"i\xDC", {0x1ECB, 0}},  /* LATIN SMALL LETTER I WITH DOT BELOW */
    {"j", {0x006A, 0}},
    {"j^", {0x0135, 0}},  /* LATIN SMALL LETTER J WITH CIRCUMFLEX */
    {"j\xF6", {0x01F0, 0}},  /* LATIN SMALL LETTER J WITH CARON */
    {"k", {0x006B, 0}},
    {"k\x1D\xB3", {0x0137, 0}},  /* LATIN SMALL LETTER K WITH CEDILLA */
    {"k\xB1", {0x1E31, 0}},  /* LATIN SMALL LETTER K WITH ACUTE */
    {"k_", {0x1E35, 0}},  /* LATIN SMALL LETTER K WITH LINE BELOW */
    {"l", {0x006C, 0}},
    {"l\xB1", {0x013A, 0}},  /* LATIN SMALL LETTER L WITH ACUTE */
    {"l\x1D\xB3", {0x013C, 0}},  /* LATIN SMALL LETTER L WITH CEDILLA */
    {"l\xF6", {0x013E, 0}},  /* LATIN SMALL LETTER L WITH CARON */
    {"l\xDC", {0x1E37, 0}},  /* LATIN SMALL LETTER L WITH DOT BELOW */
    {"l_", {0x1E3B, 0}},  /* LATIN SMALL LETTER L WITH LINE BELOW */
    {"m", {0x006D, 0}},
    {"m\xB1", {0x1E3F, 0}},  /* LATIN SMALL LETTER M WITH ACUTE */
    {"m\xBB", {0x1E41, 0}},  /* LATIN SMALL LETTER M WITH DOT ABOVE */
    {"m\xDC", {0x1E43, 0}},  /* LATIN SMALL LETTER M WITH DOT BELOW */
    {"n", {0x006E, 0}},
    {"n~", {0x00F1, 0}},  /* LATIN SMALL LETTER N WITH TILDE */
    {"n\xB1", {0x0144, 0}},  /* LATIN SMALL LETTER N WITH ACUTE */
    {"n\x1D\xB3", {0x0146, 0}},  /* LATIN SMALL LETTER N WITH CEDILLA */
    {"n\xF6", {0x0148, 0}},  /* LATIN SMALL LETTER N WITH CARON */
    {"n\xDB", {0x01F9, 0}},  /* LATIN SMALL LETTER N WITH GRAVE */
    {"n\xBB", {0x1E45, 0}},  /* LATIN SMALL LETTER N WITH DOT ABOVE */
    {"n\xDC", {0x1E47, 0}},  /* LATIN SMALL LETTER N WITH DOT BELOW */
    {"n_", {0x1E49, 0}},  /* LATIN SMALL LETTER N WITH LINE BELOW */
    {"o", {0x006F, 0}},
    {"o\xDB", {0x00F2, 0}},  /* LATIN SMALL LETTER O WITH GRAVE */
    {"o\xB1", {0x00F3, 0}},  /* LATIN SMALL LETTER O WITH ACUTE */
    {"o^", {0x00F4, 0}},  /* LATIN SMALL LETTER O WITH CIRCUMFLEX */
    {"o~", {0x00F5, 0}},  /* LATIN SMALL LETTER O WITH TILDE */
    {"o\xDF", {0x014F, 0}},  /* LATIN SMALL LETTER O WITH BREVE */
    {"o\x1D\xB1", {0x0151, 0}},  /* LATIN SMALL LETTER O WITH DOUBLE ACUTE */
    {"o\xB3", {0x01EB, 0}},  /* LATIN SMALL LETTER O WITH OGONEK */
    {"o\xF6", {0x01D2, 0}},  /* LATIN SMALL LETTER O WITH CARON */
    {"o\x1D\xDB", {0x020D, 0}},  /* LATIN SMALL LETTER O WITH DOUBLE GRAVE */
    {"o\xBB", {0x022F, 0}},  /* LATIN SMALL LETTER O WITH DOT ABOVE */
    {"o~\xB1", {0x1E4D, 0}},  /* LATIN SMALL LETTER O WITH TILDE AND ACUTE */
    {"o\xC4\xB1", {0x1E53, 0}},  /* LATIN SMALL LETTER O WITH MACRON AND ACUTE */
    {"o\xDC", {0x1ECD, 0}},  /* LATIN SMALL LETTER O WITH DOT BELOW */
    {"o\xDC^", {0x1ED9, 0}},  /* LATIN SMALL LETTER O WITH CIRCUMFLEX AND DOT BELOW */
    {"p", {0x0070, 0}},
    {"p\xB1", {0x1E55, 0}},  /* LATIN SMALL LETTER P WITH ACUTE */
    {"q", {0x0071, 0}},
    {"r", {0x0072, 0}},
    {"r\xB1", {0x0155, 0}},  /* LATIN SMALL LETTER R WITH ACUTE */
    {"r\x1D\xB3", {0x0157, 0}},  /* LATIN SMALL LETTER R WITH CEDILLA */
    {"r\xF6", {0x0159, 0}},  /* LATIN SMALL LETTER R WITH CARON */
    {"r\xBB", {0x1E59, 0}},  /* LATIN SMALL LETTER R WITH DOT ABOVE */
    {"r\xDC", {0x1E5B, 0}},  /* LATIN SMALL LETTER R WITH DOT BELOW */
    {"r_", {0x1E5F, 0}},  /* LATIN SMALL LETTER R WITH LINE BELOW */
    {"s", {0x0073, 0}},
    {"s\xB1", {0x015B, 0}},  /* LATIN SMALL LETTER S WITH ACUTE */
    {"s^", {0x015D, 0}},  /* LATIN SMALL LETTER S WITH CIRCUMFLEX */
    {"s\x1D\xB3", {0x015F, 0}},  /* LATIN SMALL LETTER S WITH CEDILLA */
    {"s\xDC", {0x1E63, 0}},  /* LATIN SMALL LETTER S WITH DOT BELOW */
    {"t", {0x0074, 0}},
    {"t\x1D\xB3", {0x0163, 0}},  /* LATIN SMALL LETTER T WITH CEDILLA */
    {"t\xF6", {0x0165, 0}},  /* LATIN SMALL LETTER T WITH CARON */
    {"t\xBB", {0x1E6B, 0}},  /* LATIN SMALL LETTER T WITH DOT ABOVE */
    {"\xDE", {0x1E6D, 0}},  /* LATIN SMALL LETTER T WITH DOT BELOW */
    {"t\xDC", {0x1E6D, 0}},  /* LATIN SMALL LETTER T WITH DOT BELOW */
    {"t_", {0x1E6F, 0}},  /* LATIN SMALL LETTER T WITH LINE BELOW */
    {"t\xBC", {0x1E97, 0}},  /* LATIN SMALL LETTER T WITH DIAERESIS */
    {"u", {0x0075, 0}},
    {"u\xDB", {0x00F9, 0}},  /* LATIN SMALL LETTER U WITH GRAVE */
    {"u\xB1", {0x00FA, 0}},  /* LATIN SMALL LETTER U WITH ACUTE */
    {"u^", {0x00FB, 0}},  /* LATIN SMALL LETTER U WITH CIRCUMFLEX */
    {"u~", {0x0169, 0}},  /* LATIN SMALL LETTER U WITH TILDE */
    {"u\xDF", {0x016D, 0}},  /* LATIN SMALL LETTER U WITH BREVE */
    {"u\xC8", {0x016F, 0}},  /* LATIN SMALL LETTER U WITH RING */
    {"u\x1D\xB1", {0x0171, 0}},  /* LATIN SMALL LETTER U WITH DOUBLE ACUTE */
    {"u\xB3", {0x0173, 0}},  /* LATIN SMALL LETTER U WITH OGONEK */
    {"u\xF6", {0x01D4, 0}},  /* LATIN SMALL LETTER U WITH CARON */
    {"u\x1D\xDB", {0x0215, 0}},  /* LATIN SMALL LETTER U WITH DOUBLE GRAVE */
    {"u\x1D^", {0x0217, 0}},  /* LATIN SMALL LETTER U WITH INVERTED BREVE ABOVE */
    {"u\x1D~", {0x1E75, 0}},  /* LATIN SMALL LETTER U WITH TILDE BELOW */
    {"u~\xB1", {0x1E79, 0}},  /* LATIN SMALL LETTER U WITH TILDE AND ACUTE */
    {"u\xDC", {0x1EE5, 0}},  /* LATIN SMALL LETTER U WITH DOT BELOW */
    {"v", {0x0076, 0}},
    {"v~", {0x1E7D, 0}},  /* LATIN SMALL LETTER V WITH TILDE */
    {"w", {0x0077, 0}},
    {"w^", {0x0175, 0}},  /* LATIN SMALL LETTER W WITH CIRCUMFLEX */
    {"x", {0x0078, 0}},
    {"y", {0x0079, 0}},
    {"y\xB1", {0x00FD, 0}},  /* LATIN SMALL LETTER Y WITH ACUTE */
    {"y\xBC", {0x00FF, 0}},  /* LATIN SMALL LETTER Y WITH DIAERESIS */
    {"y^", {0x0177, 0}},  /* LATIN SMALL LETTER Y WITH CIRCUMFLEX */
    {"y\xC4", {0x0233, 0}},  /* LATIN SMALL LETTER Y WITH MACRON */
    {"z", {0x007A, 0}},
    {"z\xB1", {0x017A, 0}},  /* LATIN SMALL LETTER Z WITH ACUTE */
    {"z\xBB", {0x017C, 0}},  /* LATIN SMALL LETTER Z WITH DOT ABOVE */
    {"z^", {0x1E91, 0}},  /* LATIN SMALL LETTER Z WITH CIRCUMFLEX */
    {"z\xDC", {0x1E93, 0}},  /* LATIN SMALL LETTER Z WITH DOT BELOW */
    {"|", {0x007C, 0}},
    {"~", {0x0303, 0}},  /* COMBINING TILDE */
    {"\x80", {0x0410, 0}},
    {"\x81", {0x0411, 0}},
    {"\x82", {0x0412, 0}},
    {"\x83", {0x0413, 0}},
    {"\x84", {0x0414, 0}},
    {"\x85", {0x0415, 0}},
    {"\x86", {0x0416, 0}},
    {"\x87", {0x0417, 0}},
    {"\x88", {0x0418, 0}},
    {"\x88\xC4", {0x04E2, 0}},  /* CYRILLIC CAPITAL LETTER I WITH MACRON */
    {"\x89", {0x0419, 0}},
    {"\x8A", {0x041A, 0}},
    {"\x8B", {0x041B, 0}},
    {"\x8C", {0x041C, 0}},
    {"\x8D", {0x041D, 0}},
    {"\x8E", {0x041E, 0}},
    {"\x8F", {0x041F, 0}},
    {"\x90", {0x0420, 0}},
    {"\x91", {0x0421, 0}},
    {"\x92", {0x0422, 0}},
    {"\x93", {0x0423, 0}},
    {"\x93\x1D\xB1", {0x04F2, 0}},  /* CYRILLIC CAPITAL LETTER U WITH DOUBLE ACUTE */
    {"\x94", {0x0424, 0}},
    {"\x95", {0x0425, 0}},
    {"\x96", {0x0426, 0}},
    {"\x97", {0x0427, 0}},
    {"\x98", {0x0428, 0}},
    {"\x99", {0x0429, 0}},
    {"\x9A", {0x042A, 0}},
    {"\x9B", {0x042B, 0}},
    {"\x9C", {0x042C, 0}},
    {"\x9D", {0x042D, 0}},
    {"\x9E", {0x042E, 0}},
    {"\x9F", {0x042F, 0}},
    {"\xA0", {0x0430, 0}},
    {"\xA1", {0x0431, 0}},
    {"\xA2", {0x0432, 0}},
    {"\xA3", {0x0433, 0}},
    {"\xA4", {0x0434, 0}},
    {"\xA5", {0x0435, 0}},
    {"\xA5\xBC", {0x0451, 0}},
    {"\xA6", {0x0436, 0}},
    {"\xA7", {0x0437, 0}},
    {"\xA8", {0x0438, 0}},
    {"\xA8\xC4", {0x04E3, 0}},  /* CYRILLIC SMALL LETTER I WITH MACRON */
    {"\xA9", {0x0439, 0}},
    {"\xAA", {0x043A, 0}},
    {"\xAB", {0x043B, 0}},
    {"\xAC", {0x043C, 0}},
    {"\xAD", {0x043D, 0}},
    {"\xAE", {0x043E, 0}},
    {"\xAE\xBC", {0x04E7, 0}},  /* CYRILLIC SMALL LETTER O WITH DIAERESIS */
    {"\xAF", {0x043F, 0}},
    {"\xB1", {0x0301, 0}},  /* COMBINING ACUTE ACCENT */
    {"\xB2", {0x00E4, 0}},  /* LATIN SMALL LETTER A WITH DIAERESIS */
    {"\xFF", {0x028C, 0}},  /* LATIN SMALL LETTER TURNED V */
    {"\xB3", {0x0328, 0}},  /* COMBINING OGONEK */
    {"\xB4", {0x01DF, 0}},  /* LATIN SMALL LETTER A WITH DIAERESIS AND MACRON */
    {"\xB5", {'c', 0x0323, 0}},  /* latin small letter c with dot below */
    {"\xB6", {0x010D, 0}},  /* LATIN SMALL LETTER C WITH CARON */
    {"\xB7", {0x010D, 0x0323, 0}},  /* latin small letter c with caron and dot below */
    {"\xB8", {0x03B4, 0}},  /* GREEK SMALL LETTER DELTA */
    {"\xB9", {0x0113, 0}},  /* LATIN SMALL LETTER E WITH MACRON */
    {"\xB9\xDB", {0x1E15, 0}},  /* LATIN SMALL LETTER E WITH MACRON AND GRAVE */
    {"\xB9\xB1", {0x1E17, 0}},  /* LATIN SMALL LETTER E WITH MACRON AND ACUTE */
    {"\xBA", {0x2016, 0}},  /* DOUBLE VERTICAL LINE */
    {"\xBB", {0x0307, 0}},  /* COMBINING DOT ABOVE */
    {"\xBC", {0x0308, 0}},  /* COMBINING DIAERESIS */
    {"\xBD", {0x025B, 0}},  /* LATIN SMALL LETTER OPEN E */
    {"\xBE", {0x02A1, 0}},  /* LATIN LETTER GLOTTAL STOP WITH STROKE */
    {"\xBF", {0x032F, 0}},  /* COMBINING INVERTED BREVE BELOW */
    {"\xC0", {0x00E7, 0}},  /* LATIN SMALL LETTER C WITH CEDILLA */
    {"\xC1", {0x0263, 0}},  /* LATIN SMALL LETTER GAMMA */
    {"\xC2", {0x0281, 0}},  /* LATIN LETTER SMALL CAPITAL INVERTED R */
    {"\xC3", {0x0127, 0}},  /* LATIN SMALL LETTER H WITH STROKE */
    {"\xC4", {0x0304, 0}},  /* COMBINING MACRON */
    {"\xC5", {0x012B, 0}},  /* LATIN SMALL LETTER I WITH MACRON */
    {"\xC6", {0x0268, 0}},  /* LATIN SMALL LETTER I WITH STROKE */
    {"\xC7", {0x0268, 0x0304, 0}},  /* latin small letter i with stroke and macron */
    {"\xC8", {0x030A, 0}},  /* COMBINING RING ABOVE */
    {"\x01\x85\xE9", {0x030A, 0}},  /* COMBINING RING ABOVE */
    {"\xC9", {0x0325, 0}},  /* COMBINING RING BELOW */
    {"\xCA", {0x1E33, 0}},  /* LATIN SMALL LETTER K WITH DOT BELOW */
    {"\xCB", {0x03BB, 0}},  /* latin small letter lambda */
    {"\xCC", {0x019B, 0}},  /* LATIN SMALL LETTER LAMBDA WITH STROKE */
    {"\xCD", {0x2011, 0}},  /* NON-BREAKING HYPHEN */
    {"*\xCE", {0x013B, 0}},
    {"\xCE", {0x019B, 0x0323, 0}},  /* latin small letter lambda with sroke and dot below */
    {"\xCF", {0x0142, 0}},  /* LATIN SMALL LETTER L WITH STROKE */
    {"\xD0", {0x0141, 0}},  /* LATIN CAPITAL LETTER L WITH STROKE */
    {"\xD1", {0x014B, 0}},  /* LATIN SMALL LETTER ENG */
    {"\xD2", {0x014D, 0}},  /* LATIN SMALL LETTER O WITH MACRON */
    {"\xD2\xDB", {0x1E51, 0}},  /* LATIN SMALL LETTER O WITH MACRON AND GRAVE */
    {"\xD2\xB1", {0x1E53, 0}},  /* LATIN SMALL LETTER O WITH MACRON AND ACUTE */
    {"\xD3", {0x00F6, 0}},  /* LATIN SMALL LETTER O WITH DIAERESIS */
    {"\xD4", {0x022B, 0}},  /* LATIN SMALL LETTER O WITH DIAERESIS AND MACRON */
    {"\xD5", {0x0254, 0}},  /* LATIN SMALL LETTER OPEN O */
    {"\xD6", {0x0254, 0x0304, 0}},  /* latin small letter open o with macron */
    {"\xD7", {0x1E57, 0}},  /* LATIN SMALL LETTER P WITH DOT ABOVE */
    {"\xD8", {'q', 0x0307, 0}},  /* latin small letter q with dot above */
    {"\xD9", {0x00DF, 0}},  /* LATIN CAPITAL LETTER SHARP S */
    {"\xDA", {0x007E, 0}},  /* TILDE */
    {"\xDB", {0x0300, 0}},  /* COMBINING GRAVE ACCENT */
    {"\xDC", {0x0323, 0}},  /* COMBINING DOT BELOW */
    {"\xDD", {0x0161, 0}},  /* LATIN SMALL LETTER S WITH CARON */
    {"\xDF", {0x0306, 0}},  /* COMBINING BREVE */
    {"\xE0", {0x0440, 0}},
    {"\xE1", {0x0441, 0}},
    {"\xE2", {0x0442, 0}},
    {"\xE3", {0x0443, 0}},
    {"\xE3\x1D\xB1", {0x04F3, 0}},  /* CYRILLIC SMALL LETTER U WITH DOUBLE ACUTE */
    {"\xE4", {0x0444, 0}},
    {"\xE5", {0x0445, 0}},
    {"\xE6", {0x0446, 0}},
    {"\xE7", {0x0447, 0}},
    {"\xE8", {0x0448, 0}},
    {"\xE9", {0x0449, 0}},
    {"\xEA", {0x044A, 0}},
    {"\xEB", {0x044B, 0}},
    {"\xEC", {0x044C, 0}},
    {"\xED", {0x044D, 0}},
    {"\xEE", {0x044E, 0}},
    {"\xEF", {0x044F, 0}},
    {"\xF0", {0x03D1, 0}},  /* GREEK THETA SYMBOL */
    {"\xF1", {0x016B, 0}},  /* LATIN SMALL LETTER U WITH MACRON */
    {"\xF2", {0x00FC, 0}},  /* LATIN SMALL LETTER U WITH DIAERESIS */
    {"\xF2\xB1", {0x01D8, 0}},  /* LATIN SMALL LETTER U WITH DIAERESIS AND ACUTE */
    {"\xF2\xF6", {0x01DA, 0}},  /* LATIN SMALL LETTER U WITH DIAERESIS AND CARON */
    {"\xF2\xDB", {0x01DC, 0}},  /* LATIN SMALL LETTER U WITH DIAERESIS AND GRAVE */
    {"\xF3", {0x01D6, 0}},  /* LATIN SMALL LETTER U WITH DIAERESIS AND MACRON */
    {"\xF4", {0x0259, 0}},  /* LATIN SMALL LETTER SCHWA */
    {"*\xF5", {0x018F, 0}},
    {"\xF5", {0x0259, 0x0304, 0}},  /* latin small letter schwa with macron */
    {"\xF6", {0x030C, 0}},  /* COMBINING CARON */
    {"\xF7", {0x02B7, 0}},  /* MODIFIER LETTER SMALL W */
    {"\xF8", {0x0266, 0}},  /* LATIN SMALL LETTER H WITH HOOK */
    {"\xF9", {0xAB53, 0}},  /* LATIN SMALL LETTER CHI */
    {"\xFA", {0x0292, 0}},
    {"\xFB", {0x01EF, 0}},  /* LATIN SMALL LETTER EZH WITH CARON */
    {"\xFC", {0x017E, 0}},  /* LATIN SMALL LETTER Z WITH CARON */
    {"\xFD", {0x0294, 0}},  /* LATIN LETTER GLOTTAL STOP */
    {"\xFE", {0x0295, 0}},  /* LATIN LETTER PHARYNGEAL VOICED FRICATIVE */
    {"\x13", {0x21C5, 0}},
    {"\x14", {0xF113, 0}},
    {"\x15", {0x00A7, 0}},  /* SECTION SIGN */
    {"\x18", {0x2191, 0}},
    {"\x19", {0x2193, 0}},
    {"\x1A", {0x2192, 0}},
    {"\x1B", {0x2190, 0}},
    {"\x1D", {0x2194, 0}},
    {"\x1E", {0xF11E, 0}},
    {"\x1F", {0xF11F, 0}},
    {"\x1D!", {0x2260, 0}},  /* NOT EQUAL TO */
    {"\x1D\"", {0x02BC, 0}},  /* MODIFIER LETTER A */
    {"\x1D#", {0x01C2, 0}},  /* LATIN LETTER ALVEOLAR CLICK */
    {"\x1D$", {0x221A, 0}},  /* SQUARE ROOT */
    {"\x1D%", {0x2018, 0}},  /* LEFT SINGLE QUOTATION MARK */
    {"\x1D&", {0x2019, 0}},  /* RIGHT SINGLE QUOTATION MARK */
    {"\x1D\'", {0x02D1, 0}},  /* MODIFIER LETTER HALF TRIANGULAR COLON */
    {"\x1D(", {0x031C, 0}},  /* LEFT HALF RING BELOW */
    {"\x1D)", {0x0339, 0}},  /* RIGHT HALF RING BELOW */
    {"\x1D+", {0x031F, 0}},  /* PLUS SIGN BELOW */
    {"\x1D,", {0x02CC, 0}},  /* MODIFIER LETTER LOW VERTICAL LINE */
    {"\x1D-", {0x0329, 0}},  /* COMBINING VERTICAL LINE BELOW */
    {"\x1D.", {0x031E, 0}},  /* TACK BELOW */
    {"\x1D/", {0x01C0, 0}},  /* LATIN LETTER DENTAL CLICK */
    {"\x1D/\x1D/", {0x01C1, 0}},  /* LATIN LETTER LATERAL CLICK */
    {"\x1D" "0", {0x0298, 0}},  /* LATIN LETTER BILABIAL CLICK */
    {"\x1D" "1", {0x02E5, 0}},  /* MODIFIER LETTER HIGH TONE BAR */
    {"\x1D" "2", {0x02E8, 0}},  /* MODIFIER LETTER MIDLOW TONE BAR */
    {"\x1D" "3", {0x02E6, 0}},  /* MODIFIER LETTER MIDHIGH TONE BAR */
    {"\x1D" "4", {0x02E7, 0}},  /* MODIFIER LETTER MIDDLE TONE BAR */
    {"\x1D" "5", {0x02E9, 0}},  /* MODIFIER LETTER LOW TONE BAR */
    {"\x1D" "9", {0x0261, 0}},  /* LATIN SMALL LETTER SCRIPT G */
    {"\x1D:", {0x02D0, 0}},  /* MODIFIER LETTER TRIANGULAR COLON */
    {"\x1D;", {0xA723, 0}},  /* LATIN SMALL LETTER EGYPTOLOGICAL ALEF */
    {"\x1D<", {0x0354, 0}},  /* COMBINING LEFT ARROWHEAD BELOW */
    {"\x1D>", {0x0355, 0}},  /* COMBINING RIGHT ARROWHEAD BELOW */
    {"\x1D?", {0x0309, 0}},  /* COMBINING HOOK ABOVE */
    {"\x1D@", {0x0251, 0}},  /* LATIN SMALL LETTER ALPHA = LATIN SMALL LETTER SCRIPT A */
    {"\x1D" "A", {0x00C6, 0}},  /* LATIN CAPITAL LETTER AE */
    {"\x1D" "B", {0xA7B5, 0}},  /* latin letter beta (standardized for upcoming Unicode 6.4) */
    {"\x1D" "C", {0x02A2, 0}},  /* LATIN LETTER REVERSED GLOTTAL STOP WITH STROKE */
    {"\x1D" "D", {0x0111, 0}},  /* LATIN SMALL LETTER D WITH STROKE */
    {"\x1D" "E", {0x0258, 0}},  /* LATIN SMALL LETTER REVERSED E */
    {"\x1D" "F", {0x0288, 0}},  /* LATIN SMALL LETTER T WITH RETROFLEX HOOK */
    {"\x1DG", {0x029B, 0}},  /* LATIN CAPITAL LETTER G WITH HOOK */
    {"\x1DH", {0x0267, 0}},  /* LATIN SMALL LETTER HENG WITH HOOK */
    {"\x1DI", {0x0269, 0}},  /* LATIN SMALL LETTER IOTA */
    {"\x1DJ", {0x025F, 0}},  /* LATIN SMALL LETTER DOTLESS J WITH STROKE */
    {"\x1DK", {0x0199, 0}},  /* LATIN SMALL LETTER K WITH HOOK */
    {"\x1DL", {0x026C, 0}},  /* LATIN SMALL LETTER L WITH BELT */
    {"\x1DM", {0x00D7, 0}},  /* MULTIPLICATION SIGN */
    {"\x1DN", {0x0273, 0}},  /* LATIN SMALL LETTER N WITH RETROFLEX HOOK */
    {"\x1DO", {0x019F, 0}},  /* LATIN CAPITAL LETTER O WITH MIDDLE TILDE */
    {"\x1DQ", {0x0262, 0}},  /* LATIN LETTER SMALL CAPITAL G */
    {"\x1DR", {0x0279, 0}},  /* LATIN SMALL LETTER TURNED R */
    {"\x1DS", {0x026D, 0}},  /* LATIN SMALL LETTER L WITH RETROFLEX HOOK */
    {"\x1DT", {0x0167, 0}},  /* LATIN SMALL LETTER T WITH STROKE */
    {"\x1DU", {0x028A, 0}},  /* LATIN SMALL LETTER UPSILON */
    {"\x1DV", {0x025E, 0}},  /* LATIN SMALL LETTER CLOSED REVERSED OPEN E */
    {"\x1DW", {0x028D, 0}},  /* LATIN SMALL LETTER TURNED W */
    {"\x1DX", {0xE0DE, 0}},  /* FIXME: ???? */
    {"\x1DY", {0x026A, 0}},  /* LATIN SMALL LETTER CAPITAL I */
    {"\x1DZ", {0x026E, 0}},  /* LATIN SMALL LETTER LEZH */
    {"\x1D[", {0x033A, 0}},  /* INVERTED BRIDGE BELOW */
    {"\x1D]", {0x032A, 0}},  /* BRIDGE BELOW */
    {"\x1D^", {0x0311, 0}},  /* COMBINING INVERTED BREVE */
    {"\x1D_", {0x0331, 0}},  /* MACRON BELOW */
    {"\x1D`", {0x0324, 0}},  /* COMBINING DIAERESIS BELOW */
    {"\x1D" "a", {0x00E6, 0}},  /* LATIN SMALL LETTER AE */
    {"\x1D" "a\xB1", {0x01FD, 0}},  /* LATIN SMALL LETTER AE WITH ACUTE */
    {"\x1D" "b", {0x0180, 0}},  /* LATIN SMALL LETTER B WITH STROKE */
    {"\x1D" "c", {0x0255, 0}},  /* LATIN SMALL LETTER C WITH CURL */
    {"\x1D" "d", {0x00F0, 0}},  /* LATIN SMALL LETTER ETH */
    {"\x1D" "e", {0x0153, 0}},  /* LATIN SMALL LIGATURE OE */
    {"\x1D" "f", {0x03B8, 0}},  /* GREEK SMALL LETTER THETA */
    {"\x1Dg", {0x01E5, 0}},  /* LATIN SMALL LETTER G WITH STROKE */
    {"\x1Dh", {0x1E2B, 0}},  /* LATIN SMALL LETTER H WITH BREVE BELOW */
    {"\x1Di", {0x0131, 0}},  /* LATIN SMALL LETTER DOTLESS I */
    {"\x1Dj", {0x0237, 0}},  /* LATIN SMALL LETTER DOTLESS J */
    {"\x1Dk", {0x029E, 0}},  /* LATIN SMALL LETTER TURNED K */
    {"\x1Dl", {0x02B8, 0}},  /* MODIFIER LETTER SMALL Y */
    {"\x1Dm", {0x026F, 0}},  /* LATIN SMALL LETTER TURNED M */
    {"\x1Dn", {0x0272, 0}},  /* LATIN SMALL LETTER N WITH LEFT HOOK */
    {"\x1Do", {0x00F8, 0}},  /* LATIN SMALL LETTER O WITH STROKE = LATIN SMALL LETTER O SLASH */
    {"\x1Do\xB1", {0x01FF, 0}},  /* LATIN SMALL LETTER O WITH STROKE AND ACUTE */
    {"\x1Dp", {0x02E4, 0}},  /* MODIFIER LETTER SMALL REVERSED GLOTTAL STOP */
    {"\x1Dq", {0x02A0, 0}},  /* LATIN SMALL LETTER Q WITH HOOK */
    {"\x1Dr", {0x027E, 0}},  /* LATIN SMALL LETTER R WITH FISHHOOK */
    {"\x1Ds", {0x0283, 0}},  /* LATIN SMALL LETTER ESH */
    {"\x1Dt", {0x00FE, 0}},  /* LATIN SMALL LETTER THORN */
    {"\x1Du", {0x0289, 0}},  /* LATIN SMALL LETTER U BAR */
    {"\x1Dv", {0x028B, 0}},  /* LATIN SMALL LETTER V WITH HOOK = LATIN SMALL LETTER SCRIPT V */
    {"\x1Dw", {0x0270, 0}},  /* LATIN SMALL LETTER TURNED M WITH LONG LEG */
    {"\x1Dx", {0x1D9A, 0}},  /* LATIN SMALL LETTER EZH WITH RETROFLEX HOOK */
    {"\x1Dy", {0x1EC9, 0}},  /* LATIN SMALL LETTER I WITH HOOK ABOVE */
    {"\x1Dz", {0x0290, 0}},  /* LATIN SMALL LETTER Z WITH RETROFLEX HOOK */
    {"\x1D{", {0x0318, 0}},  /* LEFT TACK BELOW */
    {"\x1D|", {0x02C8, 0}},  /* MODIFIER LETTER VERTICAL LINE */
    {"\x1D}", {0x0319, 0}},  /* RIGHT TACK BELOW */
    {"\x1D~", {0x0330, 0}},  /* TILDE BELOW - I changed it from diaeresis - SAS */
    {"\x1D\x80", {0x0250, 0}},  /* LATIN SMALL LETTER TURNED A */
    {"\x1D\x81", {0x029A, 0}},  /* LATIN SMALL LETTER CLOSED OPEN E */
    {"\x1D\x84", {0x0256, 0}},  /* LATIN SMALL LETTER D WITH TAIL */
    {"\x1D\x85", {0x025A, 0}},  /* LATIN SMALL LETTER SCHWA WITH HOOK */
    {"\x1D\x86", {0x027A, 0}},  /* LATIN SMALL LETTER TURNED R WITH LONG LEG */
    {"\x1D\x87", {0x025C, 0}},  /* LATIN SMALL LETTER REVERSED OPEN E */
    {"\x1D\x89", {0x029D, 0}},  /* LATIN SMALL LETTER J WITH CROSSED-TAIL */
    {"\x1D\x8E", {0x0275, 0}},  /* LATIN SMALL LETTER BARRED O */
    {"\x1D\x90", {0x027F, 0}},  /* LATIN SMALL LETTER REVERSED R WITH FISHHOOK */
    {"\x1D\x91", {0x0282, 0}},  /* LATIN SMALL LETTER S WITH HOOK */
    {"\x1D\x92", {0x031D, 0}},  /* UPWARD TACK BELOW */
    {"\x1D\x94", {0x01AD, 0}},  /* LATIN SMALL LETTER T WITH HOOK */
    {"\x1D\x96", {0x01A6, 0}},  /* LATIN LETTER YR */
    {"\x1D\x97", {0x027C, 0}},  /* LATIN SMALL LETTER R WITH LONG LEG */
    {"\x1D\xA0", {0x0252, 0}},  /* LATIN SMALL LETTER TURNED ALPHA */
    {"\x1D\xA1", {0x0253, 0}},  /* LATIN SMALL LETTER B WITH HOOK */
    {"\x1D\xA3", {0x0260, 0}},  /* LATIN SMALL LETTER G WITH HOOK */
    {"\x1D\xA4", {0x0257, 0}},  /* LATIN SMALL LETTER D WITH HOOK */
    {"\x1D\xA5", {0x0467, 0}},  /* CYRILLIC SMALL LETTER LITTLE YUS */
    {"\x1D\xA6", {0x027B, 0}},  /* LATIN SMALL LETTER TURNED R WITH HOOK */
    {"\x1D\xA7", {0x0455, 0}},  /* CYRILLIC SMALL LETTER DZE */
    {"\x1D\xA9", {0x0284, 0}},  /* LATIN SMALL LETTER DOTLESS J WITH STROKE AND HOOK */
    {"\x1D\xAB", {0x0459, 0}},  /* CYRILLIC SMALL LETTER LJE */
    {"\x1D\xAC", {0x0271, 0}},  /* LATIN SMALL LETTER M WITH HOOK */
    {"\x1D\xAD", {0x045A, 0}},  /* CYRILLIC SMALL LETTER NJE */
    {"\x1D\xAE", {0x046B, 0}},  /* CYRILLIC SMALL LETTER BIG YUS */
    {"\x1D\xB1", {0x030B, 0}},  /* COMBINING DOUBLE ACUTE ACCENT */
    {"\x01\x85\xE7", {0x030B, 0}},  /* COMBINING DOUBLE ACUTE ACCENT */
    {"\x1D\xB3", {0x0327, 0}},  /* COMBINING CEDILLA */
    {"\x1D\xBB", {0x0313, 0}},  /* COMBINING COMMA ABOVE */
    {"\x1D\xBF", {0x032E, 0}},  /* COMBINING BREVE BELOW */
    {"\x1D\xC2", {0x027D, 0}},  /* LATIN SMALL LETTER R WITH TAIL */
    {"\x1D\xC3", {0x045B, 0}},  /* CYRILLIC SMALL LETTER TSHE */
    {"\x1D\xCB", {0x028E, 0}},  /* LATIN SMALL LETTER TURNED Y */
    {"\x1D\xDA", {0x2248, 0}},  /* approximate equality */
    {"\x1D\xDB", {0x030F, 0}},  /* COMBINING DOUBLE GRAVE ACCENT */
    {"\x01\x85\xE8", {0x030F, 0}},  /* COMBINING DOUBLE GRAVE ACCENT */
    {"\x1D\xE0", {0x0281, 0}},  /* LATIN LETTER SMALL CAPITAL INVERTED R (duplicate!) */
    {"\x1D\xE1", {0x0296, 0}},  /* LATIN LETTER INVERTED GLOTTAL STOP */
    {"\x1D\xE2", {0x031E, 0}},  /* DOWN TACK BELOW (duplicate!) */
    {"\x1D\xE3", {0x0264, 0}},  /* LATIN SMALL LETTER RAMS HORN = LATIN SMALL LETTER BABY GAMMA */
    {"\x1D\xE4", {0x0278, 0}},  /* LATIN SMALL LETTER PHI */
    {"\x1D\xE5", {0x02B0, 0}},  /* MODIFIER LETTER SMALL H */
    {"\x1D\xE6", {0x0287, 0}},  /* LATIN SMALL LETTER TURNED T */
    {"\x1D\xE7", {0x0265, 0}},  /* LATIN SMALL LETTER TURNED H */
    {"\x1D\xED", {0x0454, 0}},  /* CYRILLIC SMALL LETTER UKRAINIAN IE */
    {"\x1D\xEF", {0x0463, 0}},  /* CYRILLIC SMALL LETTER YAT */
    {"\x1D\xF8", {0x0195, 0}},  /* LATIN SMALL LETTER HV */
    {"\x1D\xF9", {0x0221, 0}},  /* LATIN SMALL LETTER D WITH CURL */
    {"\x1D\xFA", {0x0452, 0}},  /* CYRILLIC SMALL LETTER DJE */
    {"\x1D\xFB", {0x0291, 0}},  /* LATIN SMALL LETTER Z WITH CURL */
    {"\x1D\xFC", {0x0236, 0}},  /* LATIN SMALL LETTER T WITH CURL */
    {"\x1D\xFD", {0x0293, 0}},  /* LATIN SMALL LETTER EZH WITH CURL */
    {"\x1D\xFE", {0x0286, 0}},  /* LATIN SMALL LETTER ESH WITH CURL */
    /* Arabic which does not fit in PersianNaskh Star font */
    {"\x01\x83\x99", {0x065C, 0}},  /* ARABIC VOWEL SIGN DOT BELOW */
    /* Greek */
    {"\x01\x83\xA4", {0x0391, 0}},  /* GREEK CAPITAL LETTER ALPHA */
    {"\x01\x83\xC2", {0x03B1, 0}},  /* GREEK SMALL LETTER ALPHA */
    {"\x01\x83\xC2\x83\x93", {0x03AC, 0}},  /* GREEK SMALL LETTER ALPHA WITH TONOS */
    {"\x01\x83\xC2\x83\xC1", {0x1F70, 0}},  /* GREEK SMALL LETTER ALPHA WITH VARIA */
    {"\x01\x83\xC2\x83\xA1", {0x1F00, 0}},  /* GREEK SMALL LETTER ALPHA WITH PSILI */
    {"\x01\x83\xC2\x83\xA0", {0x1F01, 0}},  /* GREEK SMALL LETTER ALPHA WITH DASIA */
    {"\x01\x83\xC2\x83\x92", {0x1F04, 0}},  /* GREEK SMALL LETTER ALPHA WITH PSILI AND OXIA */
    {"\x01\x83\xC2\x83\x91", {0x1F05, 0}},  /* GREEK SMALL LETTER ALPHA WITH DASIA AND OXIA */
    {"\x01\x83\xC2\x83\xC0", {0x1F06, 0}},  /* GREEK SMALL LETTER ALPHA WITH PSILI AND PERISPOMENI */
    {"\x01\x83\xC2\x83\xA0\x83\xDC", {0x1F07, 0}},  /* GREEK SMALL LETTER ALPHA WITH DASIA AND PERISPOMENI */
    {"\x01\x83\xC2~", {0x1F86, 0}},  /* GREEK SMALL LETTER ALPHA WITH PSILI AND PERISPOMENI AND YPOGEGRAMMENI */
    {"\x01\x83\xC2\x7F\xDF", {0x1FB0, 0}},  /* GREEK SMALL LETTER ALPHA WITH VRACHY */
    {"\x01\x83\xC2\x7F\xC4", {0x1FB1, 0}},  /* GREEK SMALL LETTER ALPHA WITH MACRON */
    {"\x01\x83\xC2\x83\xDC", {0x1FB6, 0}},  /* GREEK SMALL LETTER ALPHA WITH PERISPOMENI */
    {"\x01\x83\xD7", {0x1FB3, 0}},  /* GREEK SMALL LETTER ALPHA WITH YPOGEGRAMMENI */
    {"\x01\x83\xD7\x7F\xB1", {0x1FB4, 0}},  /* GREEK SMALL LETTER ALPHA WITH OXIA AND YPOGEGRAMMENI */
    {"\x01\x83\xD7~", {0x1FB7, 0}},  /* GREEK SMALL LETTER ALPHA WITH PERISPOMENI AND YPOGEGRAMMENI */
    {"\x01\x83\xA5", {0x0392, 0}},  /* GREEK CAPITAL LETTER BETA */
    {"\x01\x83\xC3", {0x03B2, 0}},  /* GREEK SMALL LETTER BETA */
    {"\x01\x83\xAA", {0x0393, 0}},  /* GREEK CAPITAL LETTER GAMMA */
    {"\x01\x83\xC8", {0x03B3, 0}},  /* GREEK SMALL LETTER GAMMA */
    {"\x01\x83\xA7", {0x0394, 0}},  /* GREEK CAPITAL LETTER DELTA */
    {"\x01\x83\xC5", {0x03B4, 0}},  /* GREEK SMALL LETTER DELTA */
    {"\x01\x83\xA8", {0x0395, 0}},  /* GREEK CAPITAL LETTER EPSILON */
    {"\x01\x83\xC6", {0x03B5, 0}},  /* GREEK SMALL LETTER EPSILON */
    {"\x01\x83\xC6\x83\x93", {0x03AD, 0}},  /* GREEK SMALL LETTER EPSILON WITH TONOS */
    {"\x01\x83\xC6\x83\xA1", {0x1F10, 0}},  /* GREEK SMALL LETTER EPSILON WITH PSILI */
    {"\x01\x83\xC6\x83\xA0", {0x1F11, 0}},  /* GREEK SMALL LETTER EPSILON WITH DASIA */
    {"\x01\x83\xC6\x83\xA0\x7F\xDB", {0x1F13, 0}},  /* GREEK SMALL LETTER EPSILON WITH DASIA AND VARIA */
    {"\x01\x83\xC6\x83\x92", {0x1F14, 0}},  /* GREEK SMALL LETTER EPSILON WITH PSILI AND OXIA */
    {"\x01\x83\xC6\x83\x91", {0x1F15, 0}},  /* GREEK SMALL LETTER EPSILON WITH DASIA AND OXIA */
    {"\x01\x83\xC6\x83\xC1", {0x1F72, 0}},  /* GREEK SMALL LETTER EPSILON WITH VARIA */
    {"\x01\x83\xBD", {0x0396, 0}},  /* GREEK CAPITAL LETTER ZETA */
    {"\x01\x83\xDB", {0x03B6, 0}},  /* GREEK SMALL LETTER ZETA */
    {"\x01\x83\xAB", {0x0397, 0}},  /* GREEK CAPITAL LETTER ETA */
    {"\x01\x83\xC9", {0x03B7, 0}},  /* GREEK SMALL LETTER ETA */
    {"\x01\x83\xC9\x83\x93", {0x03AE, 0}},  /* GREEK SMALL LETTER ETA WITH TONOS */
    {"\x01\x83\xC9\x7F\xB1", {0x03AE, 0}},  /* GREEK SMALL LETTER ETA WITH TONOS */
    {"\x01\x83\xC9\x83\xA1", {0x1F20, 0}},  /* GREEK SMALL LETTER ETA WITH PSILI */
    {"\x01\x83\xC9\x83\xA0", {0x1F21, 0}},  /* GREEK SMALL LETTER ETA WITH DASIA */
    {"\x01\x83\xC9\x83\x92", {0x1F24, 0}},  /* GREEK SMALL LETTER ETA WITH PSILI AND OXIA */
    {"\x01\x83\xC9\x83\x91", {0x1F25, 0}},  /* GREEK SMALL LETTER ETA WITH DASIA AND OXIA */
    {"\x01\x83\xC9\x83\xA0\x83\xDC", {0x1F27, 0}},  /* GREEK SMALL LETTER ETA WITH DASIA AND PERISPOMENI */
    {"\x01\x83\xC9\x83\xA1\x83\xDC", {0x1F26, 0}},  /* GREEK SMALL LETTER ETA WITH PSILI AND PERISPOMENI */
    {"\x01\x83\xC9\x83\xC1", {0x1F74, 0}},  /* GREEK SMALL LETTER ETA WITH VARIA */
    {"\x01\x83\xB9", {0x1FC3, 0}},  /* GREEK SMALL LETTER ETA WITH YPOGEGRAMMENI */
    {"\x01\x83\xB9\x83\x93", {0x1FC4, 0}},  /* GREEK SMALL LETTER ETA WITH OXIA AND YPOGEGRAMMENI */
    {"\x01\x83\xC9\x83\xDC", {0x1FC6, 0}},  /* GREEK SMALL LETTER ETA WITH PERISPOMENI */
    {"\x01\x83\xB9~", {0x1FC7, 0}},  /* GREEK SMALL LETTER ETA WITH PERISPOMENI AND YPOGEGRAMMENI */
    {"\x01\x83\xB4", {0x0398, 0}},  /* GREEK CAPITAL LETTER THETA */
    {"\x01\x83\xD2", {0x03B8, 0}},  /* GREEK SMALL LETTER THETA */
    {"\x01\x83\xAC", {0x0399, 0}},  /* GREEK CAPITAL LETTER IOTA */
    {"\x01\x83\xCA", {0x03B9, 0}},  /* GREEK SMALL LETTER IOTA */
    {"\x01\x83\xCA\x83\x93", {0x03AF, 0}},  /* GREEK SMALL LETTER IOTA WITH TONOS */
    {"\x01\x83\xCA\x7F\xB1", {0x03AF, 0}},  /* GREEK SMALL LETTER IOTA WITH TONOS */
    {"\x01\x83\xCA\x83\xC1", {0x1F76, 0}},  /* GREEK SMALL LETTER IOTA WITH VARIA */
    {"\x01\x83\xCA\x83\xA3", {0x03CA, 0}},  /* GREEK SMALL LETTER IOTA WITH DIALYTIKA */
    {"\x01\x83\xCA\x83\xA1", {0x1F30, 0}},  /* GREEK SMALL LETTER IOTA WITH PSILI */
    {"\x01\x83\xCA\x83\xA0", {0x1F31, 0}},  /* GREEK SMALL LETTER IOTA WITH DASIA */
    {"\x01\x83\xCA\x83\xC1\x83\xA0", {0x1F33, 0}},  /* GREEK SMALL LETTER IOTA WITH DASIA AND VARIA */
    {"\x01\x83\xCA\x83\xA1\x83\xDC", {0x1F36, 0}},  /* GREEK SMALL LETTER IOTA WITH PSILI AND PERISPOMENI */
    {"\x01\x83\xCA\x83\xA0\x83\xDC", {0x1F37, 0}},  /* GREEK SMALL LETTER IOTA WITH DASIA AND PERISPOMENI */
    {"\x01\x83\xCA\x83\x92", {0x1F34, 0}},  /* GREEK SMALL LETTER IOTA WITH PSILI AND OXIA */
    {"\x01\x83\xCA\x83\x91", {0x1F35, 0}},  /* GREEK SMALL LETTER IOTA WITH DASIA AND OXIA */
    {"\x01\x83\xCA\x7F\xC4", {0x1FD1, 0}},  /* GREEK SMALL LETTER IOTA WITH MACRON */
    {"\x01\x83\xCA\x83\x90", {0x1FD3, 0}},  /* GREEK SMALL LETTER IOTA WITH DIALYTIKA AND OXIA */
    {"\x01\x83\xCA\x83\xDC", {0x1FD6, 0}},  /* GREEK SMALL LETTER IOTA WITH PERISPOMENI */
    {"\x01\x83\xCA\x7F\xDF", {0x1FD0, 0}},  /* GREEK SMALL LETTER IOTA WITH VRACHY */
    {"\x01\x83\xAE", {0x039A, 0}},  /* GREEK CAPITAL LETTER KAPPA */
    {"\x01\x83\xCC", {0x03BA, 0}},  /* GREEK SMALL LETTER KAPPA */
    {"\x01\x83\xAF", {0x039B, 0}},  /* GREEK CAPITAL LETTER LAMBDA */
    {"\x01\x83\xCD", {0x03BB, 0}},  /* GREEK SMALL LETTER LAMBDA */
    {"\x01\x83\xB0", {0x039C, 0}},  /* GREEK CAPITAL LETTER MU */
    {"\x01\x83\xCE", {0x03BC, 0}},  /* GREEK SMALL LETTER MU */
    {"\x01\x83\xB1", {0x039D, 0}},  /* GREEK CAPITAL LETTER NU */
    {"\x01\x83\xCF", {0x03BD, 0}},  /* GREEK SMALL LETTER NU */
    {"\x01\x83\xBB", {0x039E, 0}},  /* GREEK CAPITAL LETTER XI */
    {"\x01\x83\xD9", {0x03BE, 0}},  /* GREEK SMALL LETTER XI */
    {"\x01\x83\xB2", {0x039F, 0}},  /* GREEK CAPITAL LETTER OMICRON */
    {"\x01\x83\xD0", {0x03BF, 0}},  /* GREEK SMALL LETTER OMICRON */
    {"\x01\x83\xD0\x83\x93", {0x03CC, 0}},  /* GREEK SMALL LETTER OMICRON WITH TONOS */
    {"\x01\x83\xD0\x7F\xB1", {0x03CC, 0}},  /* GREEK SMALL LETTER OMICRON WITH TONOS */
    {"\x01\x83\xD0\x83\xA1", {0x1F40, 0}},  /* GREEK SMALL LETTER OMICRON WITH PSILI */
    {"\x01\x83\xD0\x83\xA0", {0x1F41, 0}},  /* GREEK SMALL LETTER OMICRON WITH DASIA */
    {"\x01\x83\xD0\x83\x92", {0x1F44, 0}},  /* GREEK SMALL LETTER OMICRON WITH PSILI AND OXIA */
    {"\x01\x83\xD0\x83\x91", {0x1F45, 0}},  /* GREEK SMALL LETTER OMICRON WITH DASIA AND OXIA */
    {"\x01\x83\xD0\x83\xC1", {0x1F78, 0}},  /* GREEK SMALL LETTER OMICRON WITH VARIA */
    {"\x01\x83\xB3", {0x03A0, 0}},  /* GREEK CAPITAL LETTER PI */
    {"\x01\x83\xD1", {0x03C0, 0}},  /* GREEK SMALL LETTER PI */
    {"\x01\x83\xB5", {0x03A1, 0}},  /* GREEK CAPITAL LETTER RHO */
    {"\x01\x83\xD3", {0x03C1, 0}},  /* GREEK SMALL LETTER RHO */
    {"\x01\x83\xD3\x83\xA0", {0x1FE5, 0}},  /* GREEK SMALL LETTER RHO WITH DASIA */
    {"\x01\x83\xB6", {0x03A3, 0}},  /* GREEK CAPITAL LETTER SIGMA */
    {"\x01\x83\xD4", {0x03C3, 0}},  /* GREEK SMALL LETTER SIGMA */
    {"\x01\x83\xCB", {0x03C2, 0}},  /* GREEK SMALL LETTER FINAL SIGMA */
    {"\x01\x83\xB7", {0x03A4, 0}},  /* GREEK CAPITAL LETTER TAU */
    {"\x01\x83\xD5", {0x03C4, 0}},  /* GREEK SMALL LETTER TAU */
    {"\x01\x83\xB8", {0x03A5, 0}},  /* GREEK CAPITAL LETTER UPSILON */
    {"\x01\x83\xD6", {0x03C5, 0}},  /* GREEK SMALL LETTER UPSILON */
    {"\x01\x83\xD6\x7F\xBC\xB1", {0x03B0, 0}},  /* GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS */
    {"\x01\x83\xD6\x7F\xBC\x01\x83\x93", {0x03B0, 0}},  /* GREEK SMALL LETTER UPSILON WITH DIALYTIKA AND TONOS */
    {"\x01\x83\xD6\x7F\xBC", {0x03CB, 0}},  /* GREEK SMALL LETTER UPSILON WITH DIALYTIKA */
    {"\x01\x83\xD6\x83\x93", {0x03CD, 0}},  /* GREEK SMALL LETTER UPSILON WITH TONOS */
    {"\x01\x83\xD6\x7F\xB1", {0x03CD, 0}},  /* GREEK SMALL LETTER UPSILON WITH TONOS */
    {"\x01\x83\xD6\x83\xA1", {0x1F50, 0}},  /* GREEK SMALL LETTER UPSILON WITH PSILI */
    {"\x01\x83\xD6\x83\xA0", {0x1F51, 0}},  /* GREEK SMALL LETTER UPSILON WITH DASIA */
    {"\x01\x83\xD6\x83\x92", {0x1F54, 0}},  /* GREEK SMALL LETTER UPSILON WITH PSILI AND OXIA */
    {"\x01\x83\xD6\x83\x91", {0x1F55, 0}},  /* GREEK SMALL LETTER UPSILON WITH DASIA AND OXIA */
    {"\x01\x83\xD6\x83\xC0", {0x1F56, 0}},  /* GREEK SMALL LETTER UPSILON WITH PSILI AND PERISPOMENI */
    {"\x01\x83\xD6\x83\x9D", {0x1F57, 0}},  /* GREEK SMALL LETTER UPSILON WITH DASIA AND PERISPOMENI */
    {"\x01\x83\xD6\x83\xC1", {0x1F7A, 0}},  /* GREEK SMALL LETTER UPSILON WITH VARIA */
    {"\x01\x83\xD6\x7F\xDF", {0x1FE0, 0}},  /* GREEK SMALL LETTER UPSILON WITH VRACHY */
    {"\x01\x83\xD6\x7F\xC4", {0x1FE1, 0}},  /* GREEK SMALL LETTER UPSILON WITH MACRON */
    {"\x01\x83\xD6\x83\xDC", {0x1FE6, 0}},  /* GREEK SMALL LETTER UPSILON WITH PERISPOMENI */
    {"\x01\x83\xA9", {0x03A6, 0}},  /* GREEK CAPITAL LETTER PHI */
    {"\x01\x83\xC7", {0x03C6, 0}},  /* GREEK SMALL LETTER PHI */
    {"\x01\x83\xA6", {0x03A7, 0}},  /* GREEK CAPITAL LETTER CHI */
    {"\x01\x83\xC4", {0x03C7, 0}},  /* GREEK SMALL LETTER CHI */
    {"\x01\x83\xBC", {0x03A8, 0}},  /* GREEK CAPITAL LETTER PSI */
    {"\x01\x83\xDA", {0x03C8, 0}},  /* GREEK SMALL LETTER PSI */
    {"\x01\x83\xBA", {0x03A9, 0}},  /* GREEK CAPITAL LETTER OMEGA */
    {"\x01\x83\xD8", {0x03C9, 0}},  /* GREEK SMALL LETTER OMEGA */
    {"\x01\x83\xD8\x83\x93", {0x03CE, 0}},  /* GREEK SMALL LETTER OMEGA WITH TONOS */
    {"\x01\x83\xD8\x7F\xB1", {0x03CE, 0}},  /* GREEK SMALL LETTER OMEGA WITH TONOS */
    {"\x01\x83\xD8\x83\xA1", {0x1F60, 0}},  /* GREEK SMALL LETTER OMEGA WITH PSILI */
    {"\x01\x83\xD8\x83\xA0", {0x1F61, 0}},  /* GREEK SMALL LETTER OMEGA WITH DASIA */
    {"\x01\x83\xD8\x83\x92", {0x1F64, 0}},  /* GREEK SMALL LETTER OMEGA WITH PSILI AND OXIA */
    {"\x01\x83\xD8\x83\x91", {0x1F65, 0}},  /* GREEK SMALL LETTER OMEGA WITH DASIA AND OXIA */
    {"\x01\x83\xD8\x83\xA1\x83\xDC", {0x1F66, 0}},  /* GREEK SMALL LETTER OMEGA WITH PSILI AND PERISPOMENI */
    {"\x01\x83\xD8\x83\xA0\x83\xDC", {0x1F67, 0}},  /* GREEK SMALL LETTER OMEGA WITH DASIA AND PERISPOMENI */
    {"\x01\x83\xD8\x83\xC1", {0x1F7C, 0}},  /* GREEK SMALL LETTER OMEGA WITH VARIA */
    {"\x01\x83\xAD\x83\xA1", {0x1FA0, 0}},  /* GREEK SMALL LETTER OMEGA WITH PSILI AND YPOGEGRAMMENI */
    {"\x01\x83\xD8\x83\xDC", {0x1FF6, 0}},  /* GREEK SMALL LETTER OMEGA WITH PERISPOMENI */
    {"\x01\x83\xAD", {0x1FF3, 0}},  /* GREEK SMALL LETTER OMEGA WITH YPOGEGRAMMENI */
    {"\x01\x83\xAD\x7F\xB1", {0x1FF4, 0}},  /* GREEK SMALL LETTER OMEGA WITH OXIA AND YPOGEGRAMMENI */
    {"\x01\x83\xAD~", {0x1FF7, 0}},  /* GREEK SMALL LETTER OMEGA WITH PERISPOMENI AND YPOGEGRAMMENI */
    {"\x01\x85\xAF", {0x03DD, 0}},  /* GREEK SMALL LETTER DIGAMMA */
    {"\x01\x83\x9E", {0x00B7, 0}},  /* MIDDLE DOT (greek ano teleia) */
    {"\x01\x83\x93", {0x0301, 0}},  /* COMBINING ACUTE ACCENT (greek oxia, tonos) */
    {"\x01\x83\xC1", {0x0300, 0}},  /* COMBINING GRAVE ACCENT (greek varia) */
    {"\x01\x83\xA1", {0x0313, 0}},  /* COMBINING COMMA ABOVE (greek psili) */
    {"\x01\x83\xA0", {0x0314, 0}},  /* COMBINING REVERSED COMMA ABOVE (greek dasia) */
    {"\x01\x83\xA3", {0x0308, 0}},  /* COMBINING DIAERESIS (greek dialytika) */
    {"\x01\x83\xDC", {0x0342, 0}},  /* COMBINING GREEK PERISPOMENI */
    {"\x01\x83\x92", {0x0313, 0x0301, 0}},  /* COMBINING COMMA ABOVE + COMBINING ACUTE ACCENT */
    {"\x01\x83\x91", {0x0314, 0x0301, 0}},  /* COMBINING REVERSED COMMA ABOVE + COMBINING ACUTE ACCENT */
    {"\x01\x83\xC0", {0x0313, 0x0342, 0}},  /* COMBINING COMMA ABOVE + COMBINING GREEK PERISPOMENI */
    {"\x01\x83\x9D", {0x0314, 0x0342, 0}},  /* COMBINING REVERSED COMMA ABOVE + COMBINING GREEK PERISPOMENI */
    {"\x01\x83\x9B", {0x0314, 0x0300, 0}},  /* COMBINING REVERSED COMMA ABOVE + COMBINING GRAVE ACCENT */
    {"\x01\x83\x9C", {0x0313, 0x0300, 0}},  /* COMBINING COMMA ABOVE + COMBINING GRAVE ACCENT */
    {"\x01\x83\x90", {0x0308, 0x0301, 0}},  /* COMBINING DIAERESIS + COMBINING ACUTE ACCENT */
    {"\x01\x83\x9A", {0x0308, 0x0300, 0}}  /* COMBINING DIAERESIS + COMBINING GRAVE ACCENT */
};


static Trie trie_0[10000 / 2];
static Trie trie_1[1500 / 2];
bool tries_initialized = false;

#ifndef __WIN32__
iconv_t iconv_descriptor[4] = { 0 };
#endif


typedef struct {
    uint8_t *buffer;
    size_t length;
    size_t buffer_length;
} String;
static inline void string_ensure_space(String *str, size_t space) {
    while (space > str->buffer_length) {
        str->buffer_length <<= 1;
        str->buffer = realloc(str->buffer, str->buffer_length);
    }
}
static inline void string_append(String *str, const uint8_t *buffer, size_t len) {
    string_ensure_space(str, str->length + len + 1);
    strncpy((char *)str->buffer + str->length, (const char *)buffer, len);
    str->length += len;
}
static inline void string_append_wchar_t(String *str, uint16_t c) {
    string_ensure_space(str, str->length + sizeof(wchar_t) * 2);
    *(wchar_t *)&str->buffer[str->length] = (wchar_t)c;
    str->length += sizeof(wchar_t);
}
static inline void string_append_utf8(String *str, uint16_t c) {
    if (c <= 0x7F) {
        string_ensure_space(str, str->length + 2);
        str->buffer[str->length++] = c;
    } else if (c <= 0x7FF) {
        string_ensure_space(str, str->length + 3);
        str->buffer[str->length++] = 0xC0 | (c >> 6);
        str->buffer[str->length++] = 0x80 | (c & 0x3F);
    } else {
        string_ensure_space(str, str->length + 4);
        str->buffer[str->length++] = 0xE0 | (c >> 12);
        str->buffer[str->length++] = 0x80 | ((c & 0xFC0) >> 6);
        str->buffer[str->length++] = 0x80 | (c & 0x3F);
    }
}

static void starlingencoding_init(void) {
    DumbTrie *dumb_trie[2] = {dumb_trie_new(), dumb_trie_new()};
    unsigned n;
    Trie *trie;

    for (n = 0; n < sizeof(starlingencoding_table) / sizeof(starlingencoding_table[0]); n++) {
        typeof(*starlingencoding_table) *entry = &starlingencoding_table[n];
        const char *table_key = entry->key;
        int mode = (table_key[0] == '\x01') ? 1 : 0;
        const char *key = mode ? table_key + 1 : table_key;
        dumb_trie[mode] = dumb_trie_add(dumb_trie[mode], (const uint8_t *)key, strlen(key), n + 1);

        const wchar_t *value = entry->value;
        String str = {entry->utf8, 0, SIZE_MAX};
        wchar_t c;
        while ((c = *value++)) {
            string_append_utf8(&str, c);
        }
        entry->utf8_len = str.length;
    }

    /* Move to static memory to stop caring about freeing */
    trie = dumb_trie_compact(dumb_trie[0]);
    dumb_trie_free(dumb_trie[0]);
    memmove(trie_0, trie, trie->data * sizeof(Cell));
    trie_free(trie);

    trie = dumb_trie_compact(dumb_trie[1]);
    dumb_trie_free(dumb_trie[1]);
    memmove(trie_1, trie, trie->data * sizeof(Cell));
    trie_free(trie);

    tries_initialized = true;
}


static inline wchar_t hex(int digit) {
    return digit + ((digit > 9) ? 'A' - 10 : '0');
}


static inline void append_unicodes_wchar_t(String *result, Cell cell) {
    const wchar_t *value = starlingencoding_table[cell - 1].value;
    while (*value) {
        string_append_wchar_t(result, *value++);
    }
}
static inline void append_unicodes_utf8(String *result, Cell cell) {
    const typeof(*starlingencoding_table) *entry = &starlingencoding_table[cell - 1];
    string_append(result, entry->utf8, entry->utf8_len);
}

static uint8_t *starlingencoding_to_unicode(const uint8_t *buffer, size_t buffer_len, bool use_wchar_t) {
    if (!tries_initialized) {
        starlingencoding_init();
    }
    String result = {malloc(buffer_len << 2), 0, buffer_len << 2};
    void (*append_unicode)(String *, uint16_t) = (use_wchar_t ? string_append_wchar_t : string_append_utf8);
    void (*append_unicodes)(String *, Cell) = (use_wchar_t ? append_unicodes_wchar_t : append_unicodes_utf8);
    int mode = 0;
    int skip = 0;
    while (buffer_len) {
        if (mode == 0 || !(skip & 1)) {
            uint8_t c = *buffer;
            if ((1 <= c && c <= 7) || c == 127) {
                mode = (c == 127) ? 0 : c;
                if (skip) {
                    skip--;
                }
                buffer++, buffer_len--;
                continue;
            }
            if (c < 0x80) {
                mode = 0;
            }
        }
        if (skip) {
            skip--;
            buffer++, buffer_len--;
            continue;
        }
        if (1 <= mode && mode <= 3 && buffer_len >= 2) {
            if ((buffer[0] == 0xA1 && buffer[1] >= 0x41) || buffer[0] > 0xA1) {
                size_t result_len = (use_wchar_t ? 3 * sizeof(wchar_t) : 11 );
                string_ensure_space(&result, result.length + result_len);
                uint8_t *result_ptr = result.buffer + result.length;
#ifdef __WIN32__
                static const unsigned mode_to_cp[] = {0, 950, 54936, 932};
                if (use_wchar_t) {
                    result.length += MultiByteToWideChar(mode_to_cp[mode], 0, buffer, 2, result_ptr, 2)
                } else {
                    wchar_t wchar[2];
                    int count = MultiByteToWideChar(mode_to_cp[mode], 0, buffer, 2, wchar, 2);
                    result.length += WideCharToMultiByte(CP_UTF8, 0, wchar, count, result_ptr, 11, NULL, NULL)
                }
#else
                if (!iconv_descriptor[mode]) {
                    static const char *mode_to_iconv[] = {NULL, "BIG5", "GB18030", "SJIS"};
                    iconv_descriptor[mode] = iconv_open(mode_to_iconv[mode],
                                                        use_wchar_t ? "wchar_t" : "UTF-8");
                }
                const uint8_t *ptr = (const uint8_t *)buffer;
                size_t ptr_len = 2;
                iconv(iconv_descriptor[mode], (const char ** restrict)&ptr, &ptr_len,
                      (char **)&result_ptr, &result_len);
                result.length += result_len;
#endif
                buffer += 2, buffer_len -= 2;
                continue;
            }
        } else if (mode == 0) {
            if (buffer_len >= 8 && !strncmp((const char *)buffer, "{U+", 3) && buffer[7] == '}') {
                append_unicode(&result, strtol((const char *)buffer + 3, NULL, 16));
                buffer += 8, buffer_len -= 8;
                continue;
            }
        }
        Cell cell = trie_find_prefix(buffer, buffer_len, mode ? trie_1 : trie_0); /* non-CJK modes > 1 not used */
        if (cell) {
            append_unicodes(&result, cell);
            /* should parse that stuff for mode switches, don't just n += len(prefix) */
            skip = strlen(starlingencoding_table[cell - 1].key) - (mode ? 2 : 1);
            buffer++, buffer_len--;
        } else {
            if (mode || *buffer == '\x1D') {
                append_unicode(&result, '{');
                append_unicode(&result, 'S');
                if (mode) {
                    append_unicode(&result, hex(mode));
                }
                append_unicode(&result, hex(buffer[0] >> 8));
                append_unicode(&result, hex(buffer[0] & 15));
                if (buffer_len > 1) {
                    append_unicode(&result, hex(buffer[1] >> 8));
                    append_unicode(&result, hex(buffer[1] & 15));
                    buffer++, buffer_len--;
                }
                append_unicode(&result, '}');
                buffer++, buffer_len--;
            } else {
                append_unicode(&result, *buffer);
                buffer++, buffer_len--;
            }
        }
    }
    if (use_wchar_t) {
        *(wchar_t *)&result.buffer[result.length] = 0;
    } else {
        result.buffer[result.length] = 0;
    }

    return result.buffer;
}

void starlingencoding_deinitialize(void) {
#ifndef __WIN32__
    int mode;
    for (mode = 1; mode <= 3; mode++) {
        if (iconv_descriptor[mode]) {
            iconv_close(iconv_descriptor[mode]);
        }
    }
#endif
}

uint8_t *starlingencoding_to_utf8(const uint8_t *buffer, size_t buffer_len) {
    return starlingencoding_to_unicode(buffer, buffer_len, false);
}

wchar_t *starlingencoding_to_wchar_t(const uint8_t *buffer, size_t buffer_len) {
    return (wchar_t *)starlingencoding_to_unicode(buffer, buffer_len, true);
}

void starlingencoding_free_utf8(uint8_t *ptr) {
    free(ptr);
}

void starlingencoding_free_wchar_t(wchar_t *ptr) {
    free(ptr);
}
