# pystarlingencoding

A Python codec for the encoding used in [StarLing](https://starlingdb.org/downl.php?lan=en#soft) files.

## Installation

    pip install git+https://gitlab.com/philkrylov/pystarlingencoding.git

## Usage

    import pystarlingencoding
    codecs.register(lambda enc: pystarlingencoding.getregentry() if enc == 'starling' else None)

## License

MIT.
