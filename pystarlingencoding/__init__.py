import codecs
from typing import Tuple

from pystarlingencoding._starlingencoding import ffi, lib


__version__ = '0.1'


class Codec(codecs.Codec):
    def decode(self,
               buffer: bytes,
               errors='strict') -> Tuple[str, int]:
        result = ffi.gc(lib.starlingencoding_to_utf8(buffer, len(buffer)),
                        lib.starlingencoding_free_utf8)
        return ffi.string(result).decode('UTF-8'), len(buffer)

    def encode(self, buffer: str, errors='strict'):
        pass


def getregentry() -> codecs.CodecInfo:
    return codecs.CodecInfo(
        name='starling',
        encode=Codec().encode,
        decode=Codec().decode,
        # incrementalencoder=IncrementalEncoder,
        # incrementaldecoder=IncrementalDecoder,
        # streamwriter=StreamWriter,
        # streamreader=StreamReader,
    )
