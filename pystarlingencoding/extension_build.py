import os.path
import platform

from cffi import FFI


def qualify(filename):
    return os.path.join("starlingencoding", filename)


def read(filename):
    return open(qualify(filename)).read()


ffibuilder = FFI()

ffibuilder.cdef(read("starlingencoding.h").split('\n\n')[1])

ffibuilder.set_source("pystarlingencoding._starlingencoding",
                      '#include "starlingencoding.h"\n',
                      sources=[qualify('decode.c'),
                               qualify('trie.c')],
                      include_dirs=['pystarlingencoding'],
                      libraries=['kernel32' if platform.system() == 'Windows'
                                 else 'iconv'])

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)
